/*Custom made version of a std::vector*/

#include <iostream>

template<typename T> class vec{

    public:
        vec(): sz{0}, elem{nullptr}, space{0} {}
        explicit vec(int s): sz{s}, elem{new T[s]}, space{s}
        {
            for(int i=0; i<sz; i++) elem[i]=0;
        }

        vec(const vec& temp);
        vec& operator=(const vec& temp);

        vec(vec&& temp);
        vec& operator=(vec&& temp);

        ~vec() {delete[] elem;}

        T& operator[](int n) {return elem[n];}
        const T& operator[](int n) const {return elem[n];}

        int size() const {return sz;}
        int capacity() const {return space;}

        void resize(int newsize);
        void push_back(const T& val);
        void reserve(int newalloc);

        void add_vec(vec& temp);

    private:
        int sz;
        T* elem;
        int space;

};

template<typename T> vec<T>::vec(const vec<T>& temp)
    : sz{temp.sz}, elem{new T[temp.sz]}, space{temp.sz}
{
    //Makes a vec and copies the data from one vec to another
    int i=0;
    for(i=0;i<int(temp.sz);i++){elem[i] = temp.elem[i];}
}

template<typename T> vec<T>& vec<T>::operator=(const vec<T>& temp)
{
    //Copies the data from one vec to another
    T* ptr = new T[temp.sz];

    int i=0;
    for(i=0;i<temp.sz;i++){ptr[i] = temp.elem[i];}

    delete[] elem;
    elem = ptr;
    sz = temp.sz;
    space = temp.sz;
    return *this;
}

template<typename T> vec<T>::vec(vec<T>&& temp)
    : sz{temp.sz}, elem{temp.elem}, space{temp.sz}
{
    //Makes a vec and moves the data from one vec to another
    temp.sz=0;
    temp.elem=nullptr;
}

template<typename T> vec<T>& vec<T>::operator=(vec<T>&& temp)
{
    //Moves the data from one vec to another
    T* ptr = new T[temp.sz];

    int i=0;
    for(i=0;i<temp.sz;i++){ptr[i] = temp.elem[i];}

    delete[] elem;
    elem = ptr;
    sz = temp.sz;
    space = temp.sz;

    temp.sz=0;
    temp.elem=nullptr;

    return *this;
}

template<typename T> void vec<T>::resize(int newsize)
{
    //Sets a vec to a new size, copies data across
    reserve(newsize);
    int i=0;
    for(i=sz;i<newsize;i++){elem[sz]=0;}
    sz=newsize;
}

template<typename T> void vec<T>::push_back(const T& val)
{
    //Add a new value to vec, creating a new space if required
    if(space==0){reserve(20);}

    if(sz<=space){reserve(2*space);}
    elem[sz] = val;
    sz++;
}

template<typename T> void vec<T>::reserve(int newalloc)
{
    //Reserves more memory for a vec, which is uninitialized
    if(newalloc<=space){return;}
    T* ptr = new T [newalloc];

    int i=0;
    for(i=0;i<sz;i++){ptr[i]=elem[i];}

    delete[] elem;
    elem = ptr;
    space=newalloc;
}

template<typename T> void vec<T>::add_vec(vec& temp)
{
    int i=0;
    for(i=0;i<sz;i++){elem[i]+=temp[i];}
}

template<typename T, typename U> double multi_vec(vec<T> vt, vec<U> vu)
{
    int i=0;
    double sum;

    for(i=0;i<vt.size();i++){
        sum+=(vt[i]*vu[i]);
    }

    return sum;
}

template<typename T>
std::ostream& operator<<(std::ostream& ou, vec<T>& temp)
{
    int i=0;
    for(i=0;i<int(temp.size());i++){
        ou << temp[i];
    }
    return ou;
}
