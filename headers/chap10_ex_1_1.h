/*Using error checking upon user input, and storing and reading vectors to
check is they're the same*/

#include <iostream>
#include <vector>
#include <fstream>

namespace Data_Collection{

    struct Point{
        double x_cord;
        double y_cord;
    };

    std::istream& operator>>(std::istream& is, Point& p);
    std::ostream& operator<<(std::ostream& os,
            std::vector<Data_Collection::Point>& original_points);

    bool vector_check(std::vector<Data_Collection::Point>& original_points,
            std::vector<Data_Collection::Point>& processed_points);

    void funct_error(std::string Message);
    void store_vector(std::vector<Data_Collection::Point>& original_points);
    void open_file_store(std::vector<Data_Collection::Point>& processed_points);
}
