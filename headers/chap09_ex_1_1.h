/*Using classes to look a and hold data, here the classes also have functions
attached to them*/

#include <string>
#include <vector>
#include <iostream>

namespace Data{

    class Name_pairs{

        private:
            std::vector<double> age;
            std::vector<std::string> name;
            //Vectors to store the age and name of people

        public:
            void add_name_age(std::string nname, double aage);
            void sort_name();

        //Allows for manipulation of the storage vectors
        //Here the first appends the vectors, allowing additions to the
        //vectors, whilst the other sorts the vectors

            std::string get_name(int i) const {return name[i];}
            double get_age(int i) const {return age[i];}
            int get_size() const {return age.size();}
        //Since variables are privat we require a function to return
        //such data

            std::vector <std::string> pass_name() const {return name;}
            std::vector <double> pass_age() const {return age;}
        //Returns whole vectors, rather than just elements which the
        //above achieves
    };

void print_name_age(const Name_pairs& np);
//Takes a Name_pairs class and prints possible private variables
//Essentially a "global" ish print

std::ostream& operator<<(std::ostream& os, const Name_pairs& np);
//Does the same as above excepts places it directly in the os stream

bool operator==(const Name_pairs& a, const Name_pairs& b);
bool operator!=(const Name_pairs& a, const Name_pairs& b);
//Allows for the comparison of classes, returning true and false when
//required
}
