/*Custom template for using numbers, with some functionality developed around
operator overloading*/

#include <iostream>

template<typename M> class number{

    public:
        number(): num{0} {}
        explicit number(M n): num{n} {}

        number(const number& temp);
        number& operator=(const number& temp);

        number(number&& temp);
        number& operator=(number&& temp);

        ~number() {}

        M& get_number(){return num;}
        M& operator+(number& temp){num = num+temp.num; return num;}
        M& operator-(number& temp){num = num-temp.num; return num;}
        M& operator/(number& temp){num = num/temp.num; return num;}
        M& operator*(number& temp){num = num*temp.num; return num;}
        M& operator%(number& temp){num = num%temp.num; return num;}

    private:
        M num;

};

template<typename M>
number<M>::number(const number<M>& temp)
    : num{temp.num}
{
}

template<typename M>
number<M>& number<M>::operator=(const number<M>& temp)
{
    num = temp.num;
    return *this;
}

template<typename M>
number<M>::number(number<M>&& temp)
    : num{temp.num}
{
    temp.num=-9999;
}

template<typename M>
number<M>& number<M>::operator=(number<M>&& temp)
{
    this.num = temp.num;
    temp.num=0;
    return this;
}

template<typename M>
std::istream& operator>>(std::istream& in, number<M>& temp)
{
    in >> temp.get_number();
    return in;
}


template<typename M>
std::ostream& operator<<(std::ostream& ou, number<M>& temp)
{
    ou << temp.get_number();
    return ou;
}
