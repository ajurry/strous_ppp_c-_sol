/*Basic custom double linked list*/

#include <string>

struct God{
    std::string name;
    std::string mythology;
    std::string vehicle;
    std::string weapon;
};

class Link{
    public:

        God details;

        Link(std::string n, std::string m, std::string v, std::string w,
                Link* p=nullptr, Link* s=nullptr)
            :details{n,m,v,w}, prev{p}, succ{s}{}

        Link* insert(Link* n);
        Link* add(Link* n);
        Link* erase();
        Link* find(const std::string& s);
        const Link* find(const std::string& s) const;

        Link* advance(int n) const;
        Link* pervious() const {return prev;}

        Link* next() const {return succ;}
        Link* previous() const {return prev;}

    private:
        Link* prev;
        Link* succ;
};

void print_all(Link* p);
