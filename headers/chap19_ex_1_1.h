/*Basic template construction for manipulation*/

#include <string>
#include <vector>
#include <iostream>

template<typename T>
struct test {

    test(T vv) : val{vv} {};
    test() : val{0} {};

    T& get_val();
    T& get_val() const;

    void set_val(T vv);
    void read_val();
    void operator=(const T&);

    private:
        T val;
};

template<typename T> T& test<T>::get_val()
{
    return val;
}

template<typename T> void test<T>::set_val(T vv)
{
    val = vv;
}

template<typename T> void test<T>::operator=(const T& vv)
{
    val = vv;
}

template<typename T> void test<T>::read_val()
{
    std::cout<<"Please Input A New Variable"<<std::endl;
    std::cin>>val;
}
