/*Basic date exercise, checking day, month, year.
Not robust*/
#include <iostream>

namespace Back {

class Date {

    public:
        Date(int y, int m, int d);
        //Date class which are given initial values
        int year() const {return y;}
        int month() const {return m;}
        int day() const {return d;}
        //Functions for making the year,month and day.
        Date();

    private:
        int y;
        int m;
        int d;
    };

std::ostream& operator<<(std::ostream& os, const Date& dd);
}
