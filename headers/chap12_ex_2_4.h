/*Basic exercise to experiment with inheritance*/

#include <iostream>
#include <vector>
#include <string>

class Stats {

    public:
        virtual void print_stats();
        int get_Health() const {return Health;}
        int get_Damage() const {return Damage;}
        int get_AttackSpeed() const {return AttackSpeed;}
        int get_Range() const {return Range;}

    protected:
        Stats(int H, int D, int AS, int R);

    private:
        int Health;
        int Damage;
        int AttackSpeed;
        int Range;
};

class Archer : public Stats {

    public:
        Archer (std::string AT, int H, int D, int AS, int R);
        std::string get_ammo_type() const {return AmmoType;}
    private:
        std::string AmmoType;
};

class Mage : public Stats {

    public:
        Mage (std::string S, int H, int D, int AS, int R);
        void print_stats() const;
        std::string get_Spell() const {return Spell;}
    private:
        std::string Spell;
};
