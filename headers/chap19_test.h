/*Template behind the basic manipulation of templates*/

#include <iostream>

template<typename T> class add_test{
    public:
    int add_elements(T* temp, int sz);
};

template<typename T>
int add_test<T>::add_elements(T* temp, int sz)
{
    int i=0;
    int sum=0;
    for(i=0;i<sz;i++){sum+=temp[i];}
    return sum;
}

template<typename T,typename A = add_test<T> > class test{

    private:
        A adder;
        int sz;
        T* var;
        T total;

    public:
        explicit test (int s)
            :sz{s}, var{new T[s]}
        {
            int i=0;
            T temp;
            for(i=0;i<s;i++){
                temp = i;
                var[i] = temp;
            }
        }

        void sum_all();
};

template<typename T, typename A>
void test<T,A>::sum_all()
{
    std::cout<<adder.add_elements(var,sz)<<std::endl;
}
