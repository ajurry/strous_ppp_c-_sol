/*Basic exercise to experiment with classes*/

#include <iostream>
#include <string>

namespace People{

    class Information{

        public:
            Information(std::string n, int a);
            int get_Age() const {return Per.Age;}
            std::string get_Name() const {return Per.Name;}

        private:
            struct Person{
                std::string Name;
                int Age;
            };
            Person Per;
    };

    std::ostream& operator <<(std::ostream& os, const Information& Info);
    std::istream& operator >>(std::istream& is, Information& Info);
}
