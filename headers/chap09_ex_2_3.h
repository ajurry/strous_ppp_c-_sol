//Library.h
#include <vector>
#include <string>
#include <iostream>

namespace Library{

    enum class Genre{
        fiction=1, nonfiction, periodical, biography, children
    };

    class Book{
        public:
            Book(std::string ISBN, std::string title,
                    std::string author, std::string cdate,
                    Genre gen, int check);
            std::string get_ISBN() const {return ISBN;}
            std::string get_title() const {return title;}
            std::string get_author() const {return author;}
            std::string get_cdate() const {return cdate;}
            std::string get_gen() const;
            int get_check() const {return check;}


        private:
            std::string ISBN;
            std::string title;
            std::string author;
            std::string cdate;
            Genre gen;
            int check;
    };

    std::istream& operator>>(std::istream& is, const Book& bb);
    std::ostream& operator<<(std::ostream& os, const Book& bb);

    bool is_ISBN(std::string ISBN);
    bool operator==(const Book& a, const Book& b);
    bool operator!=(const Book& a, const Book& b);
    void checking (char a, const Book& bb);

    class Patron{
        public:
            Patron(std::string Name, std::string Number,
                    double Balance);
            std::string get_Name() const {return Name;}
            std::string get_Number() const {return Number;}
            double get_Balance() const {return Balance;}

        private:
            std::string Name;
            std::string Number;
            double Balance;
    };

    bool operator<(const Patron& pp, int a);
    std::ostream& operator<<(const Patron& pp, std::ostream& os);
}
