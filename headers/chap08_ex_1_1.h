#ifndef my_h
#define my_h

extern int foo;
//Althought external variable is said to exist it still has to be
//set in another .cpp, and so in essence means that it isn't a 
//declaration.

void print_foo();
void print(int i);

#endif /*my_h*/
