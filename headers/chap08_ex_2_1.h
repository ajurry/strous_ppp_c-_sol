#ifndef my_h
#define my_h

extern int a;
extern int b;

void swap_v(int, int);
void swap_r(int&, int&);

#endif /*my_h*/
