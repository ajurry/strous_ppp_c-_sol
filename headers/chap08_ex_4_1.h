/*More complicated use of namespaces and functions to manipulate data*/

#ifndef functions_h
#define functions_h

#include <vector>

namespace functions{
void fibonacci (int, int, std::vector<int>&, int length);
void reverse (std::vector<int>, std::vector<int>&, int length);
void swap (std::vector<int>&, int);
};

#endif
