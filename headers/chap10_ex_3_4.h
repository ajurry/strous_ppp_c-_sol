/*Creates a temperature data set and stores it to a file
It then reopens the file, takes the data, converts it to farenheit and then
display it on screen*/

#include<vector>

namespace temps{

    struct Reading{
        int hour;
        double temperature;
        char type;
    };

    void output_data();
    void input_data(std::vector<Reading>& ivector);
    void test_converter(std::vector<Reading>& data);

    double mean(std::vector<Reading>& data);
    double median(std::vector<Reading>& data);
};
