/*Experiments with tracing destuction and construction of elements
through a RAII style, here the template follows the production and loss
of elements*/

#include <iostream>

extern int count;

template<typename T> class tracer{
    public:
        tracer(): tracer_num{count}, array{new T[count]}
        {
            std::cout<<"---------------------"<<std::endl;
            std::cout<<"Creating Tracer :: "<<count<<std::endl;
        }

        tracer(const tracer& temp);
        tracer& operator=(const tracer& temp);

        ~tracer()
        {
            std::cout<<"---------------------"<<std::endl;
            std::cout<<"Destroying Tracer :: "<<tracer_num<<std::endl;
            std::cout<<"Possible Copy Of :: "<<cp<<std::endl;
            delete[] array;
        }

    private:
        int tracer_num;
        T* array;
        int cp=-1;
};

template<typename T>
tracer<T>::tracer(const tracer<T>& temp)
    :tracer_num{count}, array{temp.array}
{
    std::cout<<"---------------------"<<std::endl;
    std::cout<<"Producing A Copy Of Tracer :: "<<temp.tracer_num<<std::endl;
    cp = temp.tracer_num;
}

template<typename T>
tracer<T>& tracer<T>::operator=(const tracer<T>& temp)
{
    array = temp.array;
    std::cout<<"---------------------"<<std::endl;
    std::cout<<"Copying Tracer :: "<<temp.tracer_num<<std::endl;
    cp = temp.tracer_num;
    return *this;
}
