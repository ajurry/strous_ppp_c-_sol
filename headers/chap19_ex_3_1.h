/*More advanced version of custom vec, that uses a template for
memory allocation*/

#include <iostream>

template<typename T> class storage{

    public:
        T* store (int n);

        void dealloc_store (T* p);
        void construct_store (T& p, const T& v);
        void destroy_store (T* p);
};

template<typename T> T* storage<T>::store (int n)
{
    T* elem = new T[n];
    return elem;
}

template<typename T> void storage<T>::dealloc_store (T* p)
{
    p->~T();
}

template<typename T> void storage<T>::construct_store (T& p, const T& v)
{
    p = v;
}

template<typename T> void storage<T>::destroy_store (T* p)
{
    p=nullptr;
}

template<typename T, typename A = storage<T>> class vec{

    private:
        A alloc;
        int sz;
        T* elem;
        int space;

    public:
        vec(): sz{0}, elem{nullptr}, space{0} {}
        explicit vec(int s): sz{s}, elem{alloc.store(s)}, space{s}
        {
            for(int i=0; i<sz; i++){alloc.construct_store(elem[i],0);}
        }

        vec(const vec& temp);
        vec& operator=(const vec& temp);

        vec(vec&& temp);
        vec& operator=(vec&& temp);

        ~vec() {delete[] elem;}

        T& operator[](int n) {return elem[n];}
        const T& operator[](int n) const {return elem[n];}

        int size() const {return sz;}
        int capacity() const {return space;}

        void resize(int newsize);
        void push_back(const T& val);
        void reserve(int newalloc);

        void add_vec(vec& temp);

};

template<typename T, typename A>
vec<T,A>::vec(const vec<T,A>& temp)
    : sz{temp.sz}, elem{alloc.store(temp.sz)}, space{temp.sz}
{
    //Makes a vec and copies the data from one vec to another
    int i=0;
    for(i=0;i<int(temp.sz);i++){
        alloc.construct_store(elem[i],temp[i]);
    }
}

template<typename T, typename A>
vec<T,A>& vec<T,A>::operator=(const vec<T,A>& temp)
{
    //Copies the data from one vec to another
    T* ptr = alloc.store(temp.sz);

    int i=0;
    for(i=0;i<temp.sz;i++){
        alloc.construct_store(ptr[i],temp.elem[i]);
    }

    alloc.dealloc_store(elem);
    elem = ptr;
    sz = temp.sz;
    space = temp.sz;
    return *this;
}

template<typename T, typename A>
vec<T,A>::vec(vec<T,A>&& temp)
    : sz{temp.sz}, elem{temp.elem}, space{temp.sz}
{
    //Makes a vec and moves the data from one vec to another
    temp.sz=0;
    temp.elem=nullptr;
}

template<typename T, typename A>
vec<T,A>& vec<T,A>::operator=(vec<T,A>&& temp)
{
    //Moves the data from one vec to another
    T* ptr = alloc.store(temp.sz);

    int i=0;
    for(i=0;i<temp.sz;i++){
        alloc.construct_store(ptr[i],temp.elem[i]);
    }

    alloc.dealloc_store(elem);
    elem = ptr;
    sz = temp.sz;
    space = temp.sz;

    alloc.dealloc_store(temp.elem);
    return *this;
}

template<typename T, typename A>
void vec<T,A>::resize(int newsize)
{
    //Sets a vec to a new size, copies data across
    reserve(newsize);
    int i=0;
    for(i=sz;i<newsize;i++){
        alloc.construct_store(elem[i],0);
    }
    for(i=newsize; i<sz; i++){
        alloc.destroy_store(&elem[i]);
    }
    sz=newsize;
}

template<typename T, typename A>
void vec<T,A>::push_back(const T& val)
{
    //Add a new value to vec, creating a new space if required
    if(space==0){reserve(20);}
    if(sz<=space){reserve(2*space);}
    alloc.construct_store(elem[sz],val);
    sz++;
}

template<typename T, typename A>
void vec<T,A>::reserve(int newalloc)
{
    //Reserves more memory for a vec, which is uninitialized
    if(newalloc<=space){return;}
    T* ptr = alloc.store(newalloc);

    int i=0;
    for(i=0;i<sz;i++){alloc.construct_store(ptr[i],elem[i]);}
    alloc.dealloc_store(elem);

    elem = ptr;
    space=newalloc;
}

template<typename T, typename A>
void vec<T,A>::add_vec(vec& temp)
{
    int i=0;
    for(i=0;i<sz;i++){elem[i]+=temp[i];}
}

template<typename T, typename A, typename U>
double multi_vec(vec<T,A> vt, vec<U,A> vu)
{
    int i=0;
    double sum;

    for(i=0;i<vt.size();i++){
        sum+=(vt[i]*vu[i]);
    }

    return sum;
}

template<typename T, typename A>
std::ostream& operator<<(std::ostream& ou, vec<T,A>& temp)
{
    int i=0;
    for(i=0;i<int(temp.size());i++){
        ou << temp[i];
    }
    return ou;
}
