/*More complicated use of namespaces and functions to manipulate data*/

#ifndef functions_h
#define functions_h


#include <vector>
#include <string>

namespace functions{
    void input(std::vector<double>& age, std::vector<std::string>& name,
            int length);
    void sort_name(std::vector<double>& age,
            std::vector<double> age_copy,
            std::vector<std::string>& name,
            std::vector<std::string> name_copy,
            int length);
};

#endif
