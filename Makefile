.PHONY: default
default: all

#----------------------------------------------------------------------
#Directory Setup for construction of possible programs
#----------------------------------------------------------------------
TARGETS:=
BASEDIR:=$(abspath $(dir $(lastword $(MAKEFILE_LIST))))
BUILDDIR:=${BASEDIR}/build
BINDIR:=${BUILDDIR}/bin
SRCDIR:=${BASEDIR}/src
OBJDIR:=${BUILDDIR}/obj
DEPSDIR:=${BASEDIR}/headers
#----------------------------------------------------------------------

#----------------------------------------------------------------------
#Abbreviations of Variables used for comp,link,dep and clean
#----------------------------------------------------------------------
CXX = g++
CXXFLAGS =-Wall -std=c++14 -g -I/${DEPSDIR}
LXXFLAGS = -Wall -std=c++14
RM = rm -rf

LINK = ${CXX} ${LXXFLAGS}
COMPILE = ${CXX} ${CXXFLAGS}
#----------------------------------------------------------------------

#----------------------------------------------------------------------
#Includes of other modular makefiles used for linking
#----------------------------------------------------------------------
include src/chap03/module.mk
include src/chap04/module.mk
include src/chap05/module.mk
include src/chap06/module.mk
include src/chap08/module.mk
include src/chap09/module.mk
include src/chap10/module.mk
include src/chap11/module.mk
include src/chap12-16/module.mk
include src/chap17/module.mk
include src/chap18/module.mk
include src/chap19/module.mk
include src/chap20/module.mk
include src/chap21/module.mk

#----------------------------------------------------------------------
${OBJDIR}/%.o: ${SRCDIR}/%.cpp
	@echo "CC $(<F)"
	@mkdir -p $(@D)
	@$(COMPILE) -c $< -o $@

#Note: Here the $@ is reference to .o, and $< to .cpp

#----------------------------------------------------------------------
#Possible target options for make to use
#----------------------------------------------------------------------
.PHONY: all
all:${TARGETS}
#	@echo "$(sort $(notdir ${TARGETS}))"

.PHONY: clean cleanbin
clean:
	@$(RM) ${BUILDDIR}
cleanbin:
	@$(RM) ${BINDIR}
#----------------------------------------------------------------------
#Note: The target variable is filled when read due to the MAKEFILE_LIST
#----------------------------------------------------------------------
