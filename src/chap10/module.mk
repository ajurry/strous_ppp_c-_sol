CHAP_DIR:= $(notdir $(abspath $(dir $(lastword $(MAKEFILE_LIST)))))


QUESTION_1_SRCS := ${CHAP_DIR}/Drill/chap10_ex_1_2.cpp
QUESTION_1_SRCS += ${CHAP_DIR}/Drill/chap10_ex_1_1.cpp
QUESTION_1_OBJS := ${QUESTION_1_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap10_prog_1: ${QUESTION_1_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


QUESTION_2_SRCS := ${CHAP_DIR}/Exercises/Exercise_1/chap10_ex_2_1.cpp
QUESTION_2_OBJS := ${QUESTION_2_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap10_prog_2: ${QUESTION_2_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


QUESTION_3_SRCS := ${CHAP_DIR}/Exercises/Exercise_2/chap10_ex_3_1.cpp
QUESTION_3_SRCS += ${CHAP_DIR}/Exercises/Exercise_2/chap10_ex_3_2.cpp
QUESTION_3_SRCS += ${CHAP_DIR}/Exercises/Exercise_2/chap10_ex_3_3.cpp
QUESTION_3_OBJS := ${QUESTION_3_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap10_prog_3: ${QUESTION_3_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


QUESTION_4_SRCS := ${CHAP_DIR}/Exercises/Exercise_3/chap10_ex_4_1.cpp
QUESTION_4_OBJS := ${QUESTION_4_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap10_prog_4: ${QUESTION_4_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


QUESTION_5_SRCS := ${CHAP_DIR}/Exercises/Exercise_4/chap10_ex_5_1.cpp
QUESTION_5_OBJS := ${QUESTION_5_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap10_prog_5: ${QUESTION_5_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


QUESTION_6_SRCS := ${CHAP_DIR}/Exercises/Exercise_5/chap10_ex_6_1.cpp
QUESTION_6_OBJS := ${QUESTION_6_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap10_prog_6: ${QUESTION_6_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


TARGETS+=${BINDIR}/chap10_prog_1 \
		 ${BINDIR}/chap10_prog_2 \
		 ${BINDIR}/chap10_prog_3 \
		 ${BINDIR}/chap10_prog_4 \
		 ${BINDIR}/chap10_prog_5 \
		 ${BINDIR}/chap10_prog_6

