/*Using error checking upon user input, and storing and reading vectors to
check is they're the same*/

#include <iostream>
#include <vector>
#include <fstream>
#include <limits>
#include "chap10_ex_1_1.h"



namespace Data_Collection{

    std::istream& operator>>(std::istream& is, Point &p){

        char ch_st;
        char ch_end;
        double x=0;
        double y=0;

        is>>ch_st>>x>>y>>ch_end;
        while(ch_end!=')' || ch_st!='(' || is.fail()){
            funct_error("Bad Input, Please input via (n n)");
            is.clear();
            is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            is>>ch_st>>x>>y>>ch_end;
        }

        p.x_cord = x;
        p.y_cord = y;
        return is;
    }

    std::ostream& operator<<(std::ostream& os,
            std::vector<Data_Collection::Point>& original_points){

        int i=0;

        for(i=0;i<int(original_points.size());i++){
            os << "X_Cord:" << "\t" << original_points[i].x_cord
                << "\t" << "Y_Cord:" << "\t" << original_points[i].y_cord
                << "\n";
        }

        os << std::endl;

        return os;
    }

    bool vector_check(std::vector<Data_Collection::Point>& original_points,
            std::vector<Data_Collection::Point>& processed_points){

        int i=0;

        if (original_points.size() != processed_points.size()){
            return false;
        }

        for (i=0;i<int(original_points.size());i++){
            if(original_points[i].x_cord != processed_points[i].x_cord){
                return false;
            }
            else if(original_points[i].y_cord != processed_points[i].y_cord){
                return false;
            }
        }

        return true;
    }

    void funct_error(std::string Message){
        std::cout << "Error:" << "\t" << Message << std::endl;
    }

    void store_vector(std::vector<Data_Collection::Point>& original_points){

        int i=0;
        std::ofstream ofs("Data.txt",std::ios::out);

         for(i=0;i<int(original_points.size());i++){
            ofs << "X_Cord:" << "\t\t" << original_points[i].x_cord
                << "\t\t" << "Y_Cord:" << "\t\t" << original_points[i].y_cord
                << std::endl;
        }

        ofs.close();
    }

    void open_file_store(std::vector<Data_Collection::Point>& processed_points){

        std::ifstream ifs;
        ifs.open("Data.txt",std::ios::in);
        if (!ifs.is_open()){ funct_error("File Didn't Open");}

        double x;
        double y;
        std::string buffer;

        while(ifs>>buffer>>x>>buffer>>y){
            processed_points.push_back(Point{x,y});
        }
        ifs.close();
    }

}
