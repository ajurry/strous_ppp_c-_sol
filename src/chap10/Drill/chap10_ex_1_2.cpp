/*Using error checking upon user input, and storing and reading vectors to
check is they're the same*/

#include <vector>
#include <iostream>
#include <sstream>
#include "chap10_ex_1_1.h"


int main(){

    int elements=0;

    std::cout<<"Data input from terminal into file"<<std::endl;
    std::cout<<"Please the number of elements to be added"<<std::endl;
    std::cin>>elements;

    Data_Collection::Point Buffer;

    Buffer.x_cord = 0;
    Buffer.y_cord = 0;


    std::vector<Data_Collection::Point> original_points;
    std::vector<Data_Collection::Point> processed_points;

    std::cout<<"Please input data in the form (n n) to store numbers"<<std::endl;

    for(int i=0;i<elements;i++){

        Data_Collection::operator>>(std::cin, Buffer);
        original_points.push_back(Buffer);
        Buffer.x_cord = 0;
        Buffer.y_cord = 0;
    }

    Data_Collection::operator<<(std::cout, original_points);
    store_vector(original_points);
    open_file_store(processed_points);

    Data_Collection::operator<<(std::cout, processed_points);

    if(vector_check(original_points, processed_points)){
        std::cout<<"Vectors before and after file write are the same "<<std::endl;
    }

    return 0;
}
