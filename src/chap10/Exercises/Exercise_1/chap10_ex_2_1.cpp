/*Simple reads from a text file and adds the numbers inside*/

#include <iostream>
#include <fstream>
#include <string>


int main (){

    std::ifstream ifs;
    ifs.open("Data.txt",std::ios::in);
    int number=0;
    int sum=0;

    while(ifs>>number){
        sum += number;
    }

    std::cout<<"Total"<<"\t\t"<<sum<<std::endl;
    ifs.close();
}
