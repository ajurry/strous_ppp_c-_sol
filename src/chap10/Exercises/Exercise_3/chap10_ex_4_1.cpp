//----------------------------------------------------------------------

#include <iostream>
#include <string>
#include <vector>

//----------------------------------------------------------------------
/*
The program simply takes in a Roman numeral and converts it into a
modern decimal number, here the logic is slightly flawed.
The flaw can be seen with MMMCCCD which shouldn't be allowed.
*/
//----------------------------------------------------------------------

namespace Roman{

class Roman_Int{
    public:
        Roman_Int(std::string Roman);
        std::string get_Roman() const {return Roman;}
        void ch_Roman(std::string rom) {Roman=rom;}
    private:
        std::string Roman;
};

bool Roman_Check(std::string Rom);
int convert_Roman(Roman_Int& Rom);

//----------------------------------------------------------------------


Roman_Int::Roman_Int(std::string Rom)
    : Roman{Rom}
{
    while(!Roman_Check(Rom)){
        std::cout<<"Please Input A Valid Number"<<std::endl;
        std::cin>>Rom;
    }
    ch_Roman(Rom);
}

//----------------------------------------------------------------------

bool Roman_Check(std::string Rom)
{
    int i=0;
    int count=0;
    int multi=10;

    std::vector<int> number;

    for(i=0;i<int(Rom.length());i++){

        if(Rom[i] == 'I'){number.push_back(1);}
        else if(Rom[i] == 'V'){number.push_back(5);}
        else if(Rom[i] == 'X'){number.push_back(10);}
        else if(Rom[i] == 'L'){number.push_back(50);}
        else if(Rom[i] == 'C'){number.push_back(100);}
        else if(Rom[i] == 'D'){number.push_back(500);}
        else if(Rom[i] == 'M'){number.push_back(1000);}
        else{
            std::cout<<"Invalid Numeral"<<std::endl;
            return false;
        }
    }

    for(i=1;i<int(number.size());i++){

        if(number[i]==number[i-1]){count++;}
        if(count == 3){
            std::cout<<"Invalid Number"<<std::endl;
            return false;
        }

        if(number[i]!=number[i-1]){count = 0;}

        if(number[i]>multi*number[i-1]){
            std::cout<<"Invalid Number"<<std::endl;
            return false;
        }

        if(number[i] == number[i-1] * 2)
        {
            std::cout<<"Invalid Number"<<std::endl;
            return false;
        }

        if(number[i] == number[i-1] && (number[i] == 5 ||
                    number[i] == 50 || number[i] == 500)){
            std::cout<<"Invalid Number"<<std::endl;
            return false;
        }

        if(i==2 && (number[i-1] < number[i])){
            std::cout<<"Invalid Number"<<std::endl;
            return false;
        }
    }

    return true;
}

std::ostream& operator<<(std::ostream& os, Roman_Int& Rom)
{
    os << "Roman" << "\t" << Rom.get_Roman() << "\t\t" << "Equals"
        << "\t" << convert_Roman(Rom) <<std::endl;

    return os;
}

int convert_Roman(Roman_Int& Rom){

    int i=0;
    int total=0;
    std::string Roman;
    std::vector<int> number;

    Roman = Rom.get_Roman();

    for(i=0;i<int(Roman.length());i++){

        if(Roman[i] == 'I'){number.push_back(1);}
        else if(Roman[i] == 'V'){number.push_back(5);}
        else if(Roman[i] == 'X'){number.push_back(10);}
        else if(Roman[i] == 'L'){number.push_back(50);}
        else if(Roman[i] == 'C'){number.push_back(100);}
        else if(Roman[i] == 'D'){number.push_back(500);}
        else if(Roman[i] == 'M'){number.push_back(1000);}
        else{std::cout<<"Invalid Numeral"<<std::endl;}
     }

    if(Roman.length()==1){return number[0];}
    else{

        for(i=1;i<int(number.size());i++){

            if(number[i]>number[i-1]){total-=number[i-1];}
            else{total+=number[i-1];}

            if(i==int(number.size()-1)){total+=number[i];}
        }
    }

    return total;
}
}


int main() {

    std::string Input;
    std::cout<<"Please Input A Roman Numeral"<<std::endl;
    std::cin>>Input;

    Roman::Roman_Int Numeral(Input);
    std::cout<<Numeral<<std::endl;

    return 0;
}
