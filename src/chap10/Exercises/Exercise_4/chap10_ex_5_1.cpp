/*Takes in a text string from two different files, combines them and
then sorts them alphabetically and stores into a file*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

void VectorInput (std::vector<std::string>& Data, std::string& Fname);
void VectorOutput (std::vector<std::string>& Data, std::string& Fname);


int main() {

    int i=0;

    std::vector<std::string> DataSet1;
    std::vector<std::string> DataSet2;
    std::string Fname1 = "Data_1.txt";
    std::string Fname2 = "Data_2.txt";
    std::string Fname3 = "Output.txt";

    VectorInput (DataSet1, Fname1);
    VectorInput (DataSet2, Fname2);

    DataSet1.insert(DataSet1.end(),DataSet2.begin(),DataSet2.end());

    for(i=0;i<int(DataSet1.size());i++){
        std::cout<<DataSet1[i]<<" ";
    }

    std::cout<<std::endl;

    std::sort(DataSet1.begin(),DataSet1.end());

    for(i=0;i<int(DataSet1.size());i++){
        std::cout<<DataSet1[i]<<" ";
    }

    std::cout<<std::endl;

    VectorOutput (DataSet1, Fname3);

    return 0;
}

void VectorInput (std::vector<std::string>& Data, std::string& Fname){

    std::string Word;
    std::ifstream ist{Fname};

    if (!ist){
        std::cout<<"Cannot Open File: "<<Fname<<std::endl;
        return;
    }

    while(ist>>Word){
        Data.push_back(Word);
    }

    return;
}

void VectorOutput (std::vector<std::string>& Data, std::string& Fname){

    int i=0;
    std::ofstream ost{Fname};

    for(i=0;i<int(Data.size());i++){
        ost<<Data[i]<<" ";
    }
    ost<<std::endl;
}
