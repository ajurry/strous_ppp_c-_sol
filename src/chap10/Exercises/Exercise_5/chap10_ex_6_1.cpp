/*Takes in a file with numbers seperated by arbituary text, it then
extracts the numbers and places them into another file, it also sums
the number that it found*/

#include <iostream>
#include <fstream>
#include <string>

int main(){

    int total=0;
    int i=0;
    bool check=false;

    std::string words;
    std::string Ifile;

    Ifile = "Input.txt";
    std::ifstream ifs {Ifile};

    if(!ifs){std::cout<<"ERROR FILE DID NOT OPEN"<<"\n"<<std::endl;}


    while(ifs>>words){

        for(i=0;i<int(words.length());i++){
            if(int(words[i]) < 48 || int(words[i])>57){
                check = false;
                break;
            }
            check = true;
        }

        if(check==true){
            total += std::stoi(words);
        }
    }

    std::cout<<"TOTAL: "<<total<<std::endl;
    ifs.close();

}
