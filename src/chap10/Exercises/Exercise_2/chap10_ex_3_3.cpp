/*Creates a temperature data set and stores it to a file
It then reopens the file, takes the data, converts it to farenheit and then
display it on screen*/

#include "chap10_ex_3_4.h"
#include <iostream>

int main (){

    std::vector<temps::Reading> data;

    temps::output_data();
    temps::input_data(data);

    std::cout<<"Mean:"<<"\t\t"<<temps::mean(data) <<"\n"
        <<"Median:"<<"\t\t"<<temps::median(data)<<"\n"
        <<std::endl;

    int i=0;

    temps::test_converter(data);

    std::cout<<"Hour"<<"\t\t"<<"Temperature"<<"\t\t"<<std::endl;
    for(i=0;i<int(data.size());i++){
        std::cout<<data[i].hour<<"\t\t"
            <<data[i].temperature<<"\t\t"
            <<data[i].type<<std::endl;
    }
    return 0;
}
