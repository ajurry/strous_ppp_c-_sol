/*Creates a temperature data set and stores it to a file
It then reopens the file, takes the data, converts it to farenheit and then
display it on screen*/

#include "chap10_ex_3_4.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

namespace temps{

    void output_data(){

    std::ofstream ofs;
    ofs.open("raw_temps.txt",std::ios::out);

    int i=0;
    int readings=51;
    double temperature=100;
    char type = 'c';

    for(i=1;i<readings;i++){
        ofs<<std::setprecision(3)<<std::setw(7)<<std::fixed
            <<"Hour"<<"\t\t"<<i<<"\t\t"
            <<"Temperature"<<"\t\t"
            <<double(temperature/(i))<<"\t\t\t"<<type
            <<std::endl;
    }

    ofs.close();
    }

    void input_data(std::vector<Reading>& ivector){

        std::ifstream ifs;
        ifs.open("raw_temps.txt",std::ios::in);

        std::string buffer;

        int hour;
        double temp;
        char type;

        while(ifs>>buffer>>hour>>buffer>>temp>>type){
            ivector.push_back(Reading{hour,temp,type});
        }
    }

};
