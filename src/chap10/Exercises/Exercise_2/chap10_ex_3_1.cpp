/*Creates a temperature data set and stores it to a file
It then reopens the file, takes the data, converts it to farenheit and then
display it on screen*/

#include "chap10_ex_3_4.h"
#include <vector>
#include <algorithm>

namespace temps{

    double mean(std::vector<Reading>& data){

        int i=0;
        double mmean=0;

        for(i=0;i<int(data.size());i++){
            mmean += data[i].temperature;
        }

        mmean /= int(data.size());

        return mmean;
    }

    bool compare_number(Reading& a, Reading& b){
        return (a.temperature < b.temperature);
    }

    double median(std::vector<Reading>& data){

        int middle=0;

        std::sort(data.begin(), data.end(), compare_number);

        middle = int(data.size())/2;

        return data[middle].temperature;
    }

    void test_converter(std::vector<Reading>& data){

        int i=0;

        for(i=0;i<int(data.size());i++){
            if(data[i].type == 'c'){
                data[i].temperature = (data[i].temperature*1.8) + 32;
                data[i].type = 'f';
            }
        }
    }

};
