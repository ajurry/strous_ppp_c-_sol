/*Basic exercise to experiment with classes*/

#include "chap12_ex_1_1.h"

#include <iostream>
#include <string>

namespace People{

    Information::Information(std::string n, int a)
        :Per{n,a}
    {
        std::string nallowed = ";:\"'[]*&^%$#@!?";

        while(a < 0 || a >=150){
            std::cout<<"Please Input a Valid Age"<<std::endl;
            std::cin>>a;
        }

        for(char let : n){
            for(char ele : nallowed){
                if (let == ele){
                    std::cout<<"ERROR INVALID CHARACTER"<<std::endl;
                }
            }
        }
    }

    std::ostream& operator <<(std::ostream& os, const Information& Info){

        os << "Name: " << Info.get_Name() << '\n'
            << "Age: " << Info.get_Age() << std::endl;

        return os;
    }

    std::istream& operator >>(std::istream& is, Information& Info){

        std::string n;
        int a;

        std::cout<<"Please Input A Name"<<std::endl;
        is>>n;
        std::cout<<"Please Input An Age"<<std::endl;
        is>>a;

        Info = Information(n,a);

        return is;
    }
}
