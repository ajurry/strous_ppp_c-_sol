/*Basic exercise to experiment with classes*/

#include "chap12_ex_1_1.h"

#include <iostream>
#include <string>

int main(){

    People::Information PER002("Andy", 22);

    People::operator<<(std::cout, PER002);

    People::operator>>(std::cin, PER002);

    People::Information PER001("Goofy", 63);

    People::operator<<(std::cout, PER001);

    People::operator<<(std::cout, PER002);

    return 0;

}
