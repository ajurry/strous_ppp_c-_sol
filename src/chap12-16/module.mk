CHAP_DIR:= $(notdir $(abspath $(dir $(lastword $(MAKEFILE_LIST)))))


QUESTION_1_SRCS := ${CHAP_DIR}/chap15-drill-class/chap12-16_ex_1_1.cpp
QUESTION_1_SRCS += ${CHAP_DIR}/chap15-drill-class/chap12-16_ex_1_2.cpp
QUESTION_1_OBJS := ${QUESTION_1_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap12-16_prog_1: ${QUESTION_1_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_2_SRCS := ${CHAP_DIR}/Inheritance/chap12-16_ex_2_1.cpp
QUESTION_2_SRCS += ${CHAP_DIR}/Inheritance/chap12-16_ex_2_2.cpp
QUESTION_2_SRCS += ${CHAP_DIR}/Inheritance/chap12-16_ex_2_3.cpp
QUESTION_2_SRCS += ${CHAP_DIR}/Inheritance/chap12-16_ex_2_5.cpp
QUESTION_2_OBJS := ${QUESTION_2_SRCS:%.cpp=${OBJDIR}/%.o}

$(info $${QUESTION_2_SRCS} is [${QUESTION_2_SRCS}])
$(info $${QUESTION_2_OBJS} is [${QUESTION_2_OBJS}])

${BINDIR}/chap12-16_prog_2: ${QUESTION_2_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

TARGETS+=${BINDIR}/chap12-16_prog_1 \
		 ${BINDIR}/chap12-16_prog_2
