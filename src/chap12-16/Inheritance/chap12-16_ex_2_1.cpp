/*Basic exercise to experiment with inheritance*/
//Archer

#include "chap12_ex_2_4.h"

#include <iostream>
#include <string>

Archer::Archer(std::string AT, int H, int D, int AS, int R)
    :  Stats(H,D,AS,R), AmmoType{AT}
{
}
