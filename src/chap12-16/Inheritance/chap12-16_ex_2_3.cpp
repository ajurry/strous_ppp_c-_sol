/*Basic exercise to experiment with inheritance*/
//Stats.cpp

#include "chap12_ex_2_4.h"

#include <iostream>
#include <string>

Stats::Stats(int H, int D, int AS, int R)
    :Health{H}, Damage{D}, AttackSpeed{AS}, Range{R}
{
}

void Stats::print_stats(){
    std::cout << "Health: " << Stats::get_Health() << "\n"
        << "Damage: " << Stats::get_Damage() << "\n"
        << "AttackSpeed: " << Stats::get_AttackSpeed() << "\n"
        << "Range: " << Stats::get_Range() << std::endl;
}
