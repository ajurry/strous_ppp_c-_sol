/*Basic exercise to experiment with inheritance*/
//Interface.cpp

#include "chap12_ex_2_4.h"

#include <iostream>
#include <string>

int main() {

    std::cout<<std::endl;
    Archer Frank("Piercing",200,50,3,20);
    Mage Bob("Fire Ball",200,5,1,20);

    Bob.print_stats();
    std::cout<<std::endl;
    Frank.print_stats();

    return 0;
}
