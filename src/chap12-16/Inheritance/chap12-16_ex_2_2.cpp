/*Basic exercise to experiment with inheritance*/
//Mage.cpp

#include "chap12_ex_2_4.h"

#include <string>
#include <iostream>

Mage::Mage(std::string S, int H, int D, int AS, int R)
    : Stats(H,D,AS,R), Spell {S}
{
}

void Mage::print_stats() const{
    std::cout << "Health: " << Stats::get_Health() << '\n'
        << "Damage: " << Stats::get_Damage() << '\n'
        << "AttackSpeed: " << Stats::get_AttackSpeed() << '\n'
        << "Range: " << Stats::get_Range() << '\n'
        << "Spell: " << Mage::get_Spell() << std::endl;
}
