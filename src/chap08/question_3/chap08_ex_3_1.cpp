/*Uses of namesapces to split elements up*/

#include <iostream>

namespace X {
    int var;
    void print();
};

void X::print(){
    std::cout << "Var Values Is:" << X::var << std::endl;
}

namespace Y {
    int var;
    void print();
};

void Y::print(){
    std::cout << "Var Values Is:" << Y::var << std::endl;
}

namespace Z {
    int var;
    void print();
};

void Z::print(){
    std::cout << "Var Values Is:" << Z::var << std::endl;
}

int main(){
    X::var = 7;
    X::print();
    using namespace Y;
    var = 9;
    print();
    {
        using Z::var;
        using Z::print;
        var = 11;
        print();
    }
    print();
    X::print();
    return 0;
}
