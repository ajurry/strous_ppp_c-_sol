CHAP_DIR:= $(notdir $(abspath $(dir $(lastword $(MAKEFILE_LIST)))))


QUESTION_1_SRCS := ${CHAP_DIR}/question_1/chap08_ex_1_2.cpp
QUESTION_1_SRCS += ${CHAP_DIR}/question_1/chap08_ex_1_1.cpp

QUESTION_1_OBJS := ${QUESTION_1_SRCS:%.cpp=${OBJDIR}/%.o}

${BINDIR}/chap08_prog_1: ${QUESTION_1_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


QUESTION_2_SRCS := ${CHAP_DIR}/question_2/chap08_ex_2_2.cpp
QUESTION_2_SRCS += ${CHAP_DIR}/question_2/chap08_ex_2_1.cpp

QUESTION_2_OBJS := ${QUESTION_2_SRCS:%.cpp=${OBJDIR}/%.o}

${BINDIR}/chap08_prog_2: ${QUESTION_2_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


QUESTION_3_SRCS := ${CHAP_DIR}/question_3/chap08_ex_3_1.cpp

QUESTION_3_OBJS := ${QUESTION_3_SRCS:%.cpp=${OBJDIR}/%.o}

${BINDIR}/chap08_prog_3: ${QUESTION_3_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


QUESTION_4_SRCS := ${CHAP_DIR}/question_4/chap08_ex_4_2.cpp
QUESTION_4_SRCS += ${CHAP_DIR}/question_4/chap08_ex_4_1.cpp

QUESTION_4_OBJS := ${QUESTION_4_SRCS:%.cpp=${OBJDIR}/%.o}

${BINDIR}/chap08_prog_4: ${QUESTION_4_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


QUESTION_5_SRCS := ${CHAP_DIR}/question_5/chap08_ex_5_2.cpp
QUESTION_5_SRCS += ${CHAP_DIR}/question_5/chap08_ex_5_1.cpp

QUESTION_5_OBJS := ${QUESTION_5_SRCS:%.cpp=${OBJDIR}/%.o}

${BINDIR}/chap08_prog_5: ${QUESTION_5_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


TARGETS+=${BINDIR}/chap08_prog_1 \
		 ${BINDIR}/chap08_prog_2 \
		 ${BINDIR}/chap08_prog_3 \
		 ${BINDIR}/chap08_prog_4 \
		 ${BINDIR}/chap08_prog_5
