/*More complicated use of namespaces and functions to manipulate data*/

#include <iostream>
#include <vector>
#include <string>
#include "chap08_ex_5_1.h"

int main(){

    int length = 0;
    int i = 0;
    char ans;

    std::cout << "Please input the number of people to be storred"
        << std::endl;
    std::cin >> length;

    std::vector<double> age (length,0);
    std::vector<std::string> name (length,"");

    functions::input(age, name, length);

    for(i=0;i<length;i++){
        std::cout << name[i] << " :: " << age[i] << std::endl;
    }

    std::cout << "Would you like to sort?(Y/N)" << std::endl;
    std::cin >> ans;
    while(ans != 'Y' && ans != 'N'){
        std::cout<<ans<<std::endl;
        std::cout<<(ans == 'Y')<<std::endl;
        std::cout<<"Please input a valid option"<<std::endl;
        std::cin >> ans;
    }

    if(ans == 'Y'){
    std::cout<<"Sorting By Name"<<std::endl;
    functions::sort_name(age, age, name, name, length);
    for(i=0;i<length;i++){
        std::cout << name[i] << " :: " << age[i] << std::endl;
    }
    }

    return 0;
}
