/*More complicated use of namespaces and functions to manipulate data*/

#include "chap08_ex_5_1.h"
#include <vector>
#include <iostream>
#include <string>
#include <algorithm>

namespace functions {
    void input(std::vector<double>& age, std::vector<std::string>& name,
            int length);
    void sort_name(std::vector<double>& age,
            std::vector<double> age_copy,
            std::vector<std::string>& name,
            std::vector<std::string> name_copy,
            int length);
};

void functions::input(std::vector<double>& age, std::vector<std::string>& name,
        int length)
{
    int i=0;

    std::cout << "Please input the names of the people" << std::endl;

    for(i=0;i<length;i++){
        std::cin >> name[i];
    }

    std::cout << "Please input the corresponding ages" << std::endl;

    for(i=0;i<length;i++){
        std::cin >> age[i];
    }
}

void functions::sort_name(std::vector<double>& age,
        std::vector<double> age_copy,
        std::vector<std::string>& name,
        std::vector<std::string> name_copy,
        int length)
{
    int i = 0;
    int j =0;

    std::sort(name.begin(), name.end());

    for(i=0; i<length; i++){
        for(j=0; j<length; j++){
            if(name[i] == name_copy[j]){
                age[i] = age_copy[j];
            }
        }
    }
}
