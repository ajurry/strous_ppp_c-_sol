/*More complicated use of namespaces and functions to manipulate data*/

#include <iostream>
#include <vector>
#include "chap08_ex_4_1.h"

int main(){

    int length = 0;
    int i = 0;
    int x = 1;
    int y = 2;

    std::cout << "Please input the number of Fib. numbers to be found"
        << std::endl;
    std::cin >> length;

    std::vector<int> fib (length,0);
    functions::fibonacci(x, y, fib, length);

    std::vector<int> rev (length,0);

    functions::reverse (fib, rev, length);

    for (i=0; i<length; i++){
        std::cout << "FV " << i << " :: " << fib[i]
            << " RV " << " :: " << rev[i] << std::endl;
    }

    functions::swap (fib, length);
    for (i=0; i<length; i++){
        std::cout << "FV " << i << " :: " << fib[i]
            << " RV " << " :: " << rev[i] << std::endl;
    }

    return 0;
}
