/*More complicated use of namespaces and functions to manipulate data*/

#include "chap08_ex_4_1.h"
#include <vector>

namespace functions {
    void fibonacci(int x, int y, std::vector<int>& fib, int length);
    void reverse(std::vector<int> fib, std::vector<int>& rev, int length);
    void swap (std::vector<int>& fib, int length);
};

void functions::fibonacci (int x, int y, std::vector<int>& fib, int length)
{
    int i = 0;
    fib[0] = x;
    fib[1] = y;

    for(i=2; i<length; i++){
        fib[i] = fib[i-1] + fib[i-2];
    }
}

void functions::reverse (std::vector<int> fib, std::vector<int>& rev,
        int length)
{
    int i = 0;
    for(i=0;i<length;i++){
        rev[length-i-1] = fib[i];
    }
}

void functions::swap (std::vector<int>& fib, int length)
{
    int i=0;
    int half = (length / 2);
    int swap = 0;

    for(i=0;i<half;i++){
        swap = fib[length-i-1];
        fib[length-i-1] = fib[i];
        fib[i] = swap;
    }
}
