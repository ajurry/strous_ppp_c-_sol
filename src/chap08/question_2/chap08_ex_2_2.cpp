/*Basic example of splitting .cpp and .h*/

#include "chap08_ex_2_1.h"
#include <iostream>

int a;
int b;

int main(){
    std::cout << "Please input a value for a" << std::endl;
    std::cin >> a;
    std::cout << "Please input a value for b" << std::endl;
    std::cin >> b;

    swap_v(a,b);
    std::cout << "a : " << a << " b : " << b << std::endl;

    swap_r(a,b);
    std::cout << "a : " << a << " b : " << b << std::endl;

    return 0;
}
