/*Basic example of splitting .cpp and .h*/

#include <iostream>
#include "chap08_ex_2_1.h"

void swap_v(int a, int b)
{
    int temp;
    temp = a;
    a = b;
    b = temp;
}

void swap_r(int& a, int& b)
{
    int temp;
    temp = a;
    a = b;
    b = temp;
}
