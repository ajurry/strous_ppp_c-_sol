CHAP_DIR:= $(notdir $(abspath $(dir $(lastword $(MAKEFILE_LIST)))))


QUESTION_1_SRCS := ${CHAP_DIR}/drills/drill_1/chap21_drill_1.cpp
QUESTION_1_OBJS := ${QUESTION_1_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap21_prog_1: ${QUESTION_1_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


QUESTION_2_SRCS := ${CHAP_DIR}/drills/drill_2/chap21_drill_2.cpp
QUESTION_2_OBJS := ${QUESTION_2_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap21_prog_2: ${QUESTION_2_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


QUESTION_3_SRCS := ${CHAP_DIR}/drills/drill_3/chap21_drill_3.cpp
QUESTION_3_OBJS := ${QUESTION_3_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap21_prog_3: ${QUESTION_3_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


QUESTION_4_SRCS := ${CHAP_DIR}/exercises/exercise_1/chap21_ex_1.cpp
QUESTION_4_OBJS := ${QUESTION_4_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap21_prog_4: ${QUESTION_4_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


QUESTION_5_SRCS := ${CHAP_DIR}/exercises/exercise_2/chap21_ex_2.cpp
QUESTION_5_OBJS := ${QUESTION_5_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap21_prog_5: ${QUESTION_5_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


QUESTION_6_SRCS := ${CHAP_DIR}/exercises/exercise_3/chap21_ex_3.cpp
QUESTION_6_OBJS := ${QUESTION_6_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap21_prog_6: ${QUESTION_6_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


TARGETS+=${BINDIR}/chap21_prog_1 \
		 ${BINDIR}/chap21_prog_2 \
		 ${BINDIR}/chap21_prog_3 \
		 ${BINDIR}/chap21_prog_4 \
		 ${BINDIR}/chap21_prog_5 \
		 ${BINDIR}/chap21_prog_6
