/*Custom count and count_if templates that take in a start and
finish of a particular data structure*/

#include <iostream>
#include <vector>

template <typename In, typename U>
int count(In start, In finish, const U& value)
{
    int total=0;
    while (start!=finish){
        ++start;
        if(*start == value){
            total+=1;
        }
    }

    return total;
}

template <typename In, typename Pred>
int count_if(In start, In finish, const Pred pred)
{
    int total=0;
    while(start!=finish){
        ++start;
        if(pred(*start)){
            total+=1;
        }
    }

    return total;
}

class less_eq {
private:
    int value;
public:
    less_eq(int num): value(num) {}
    bool operator()(int cmp) const {return cmp<=value;}
};

int main() {

    int i;
    int sz=20;
    std::vector<int> data;
    for(i=0;i<sz;++i){
        if(i%2==0){
            data.push_back(0);
        }
        else if(i%3==0){
            data.push_back(1);
        }
        else{
            data.push_back(2);
        }
    }

    std::cout<<"=========================================="<<std::endl;
    std::cout << count(data.begin(),data.end(), 0)
        << " " << count(data.begin(), data.end(), 1)
        << " " << count(data.begin(), data.end(), 2)
        << " " << count_if(data.begin(), data.end(), less_eq(1))
        << std::endl;
    std::cout<<"=========================================="<<std::endl;

    return 0;
}
