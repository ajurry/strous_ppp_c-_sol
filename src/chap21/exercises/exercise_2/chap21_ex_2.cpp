/*Custom made binary_search which can work on both List and Vector*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <sstream>
#include <list>

template<typename T, typename U>
auto binary_search(T& objc, U srch_crit){

    int sz_h = ((objc.size()/2)+0.5);
    int old_h;
    auto current = objc.begin();
    std::advance(current,sz_h);

    if(objc.size()%2==0){
        if(srch_crit>*current){
            --current;
        }
    }

    while(srch_crit!=*current){

        old_h = sz_h;
        sz_h = ((double)sz_h/2)+0.5;

        if(sz_h==old_h){
            std::cout<<"Element Not Found ("<<srch_crit<<")"<<std::endl;
            return;
        }
        else if(srch_crit<*current){
            std::advance(current,-sz_h);
            }
        else if(srch_crit>*current){
            std::advance(current,sz_h);
            }
    }

    std::cout<<"Element Found: "<<*current<<std::endl;
    return;
}

int main () {

    int i=0;
    int sz=12;
    std::vector<int> vec_1;

    while(i!=sz){
        vec_1.push_back(i);
        ++i;
    }

    std::sort(vec_1.begin(),vec_1.end());

    i=0;
    while(i!=sz){
        binary_search(vec_1,i);
        ++i;
    }

    std::list<std::string> list_1;

    i=0;
    std::stringstream sstr;
    while(i!=sz){
        sstr << "a";
        sstr << i;
        list_1.push_back(sstr.str());
        ++i;
        sstr.str("");
    }

    list_1.sort();

    auto ptr = list_1.begin();
    while(ptr!=list_1.end()){
        std::cout<<*ptr<<std::endl;
        ++ptr;
    }

    binary_search(list_1,"a3");

    return 0;
}
