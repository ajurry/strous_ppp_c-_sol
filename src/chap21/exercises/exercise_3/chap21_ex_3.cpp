/*Sorts and counts frequency of words inside a text file*/

#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

struct val_comparison{
        bool operator()(const std::pair<std::string,int>& a,
                const std::pair<std::string,int>& b) const{
            return b.second<a.second;
        }
};

int main() {

    std::ifstream ifs("input_chap21_ex_3.txt");

    std::map<std::string,int> words;
    std::string temp;
    std::vector<std::pair<std::string,int>> sorter;

    while(ifs >> temp){
        ++words[temp];
    }

    ifs.close();

    for(const auto& p : words){
        std::cout<<p.first<<": "<<p.second<<"\n";
    }

    std::cout<<"==========================================="<<std::endl;

    for(auto ptr = words.begin(); ptr!=words.end(); ++ptr){
        sorter.push_back(*ptr);
    }

    std::sort(sorter.begin(),sorter.end(),val_comparison());
    for(auto ptr = sorter.begin(); ptr!=sorter.end(); ++ptr)
    {
        std::cout<<ptr->first<<": "<<ptr->second<<"\n";
    }


    return 0;
}
