/*Exercise on custom structs combined with algorithms*/

#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>
#include <list>

struct Item {
    std::string name;
    int iid;
    double value;

    Item(std::string nname=" ", int iiid=-1000, double vvalue=0)
        :name{nname}, iid{iiid}, value{vvalue} {}

    friend bool operator==(const Item& a, const Item& b);

    ~Item(){return;}
};

bool operator==(const Item& a, const Item& b){
    return (a.name == b.name || a.iid == b.iid);
}


struct Cmp_by_name {
    bool operator()(const Item& a, const Item& b) const
    {return a.name < b.name;}
};

struct Cmp_by_iid {
    bool operator()(const Item& a, const Item& b) const
    {return a.iid < b.iid;}
};

struct Cmp_by_value {
    bool operator()(const Item& a, const Item& b) const
    {return a.value < b.value;}
};

void data_in(std::vector<Item>& d_in){

    std::ifstream in;
    in.open("input_chap21_1.txt");

    std::string n_temp;
    int i_temp;
    double v_temp;

    if (in.is_open()){
        std::cout<<"Opening File"<<std::endl;
    }
    else{
        std::cout<<"Error, Cannot Open File"<<std::endl;
    }

    while(in>>n_temp>>i_temp>>v_temp){
        d_in.push_back(Item(n_temp,i_temp,v_temp));
    }

    in.close();
}

void data_in(std::list<Item>& d_in){

    std::ifstream in;
    in.open("input_chap21_1.txt");

    std::string n_temp;
    int i_temp;
    double v_temp;

    if (in.is_open()){
        std::cout<<"Opening File"<<std::endl;
    }
    else{
        std::cout<<"Error, Cannot Open File"<<std::endl;
    }

    while(in>>n_temp>>i_temp>>v_temp){
        d_in.push_back(Item(n_temp,i_temp,v_temp));
    }

    in.close();
}


std::ostream& operator<<(std::ostream& ou, Item& d_item){

    ou<<d_item.name<<' '<<d_item.iid<<' '<<d_item.value<<'\n';
    return ou;
}

std::ostream& operator<<(std::ostream& ou, std::vector<Item>& d_in){

    for(auto ptr=d_in.begin(); ptr!=d_in.end(); ++ptr){
        ou<<*ptr;
    }

    return ou;
}

std::ostream& operator<<(std::ostream& ou, std::list<Item>& d_in){

    for(auto ptr=d_in.begin(); ptr!=d_in.end(); ++ptr){
        ou<<*ptr;
    }

    return ou;
}

int main() {

    std::vector<Item> data;
    data_in(data);

    std::cout<<data;
    std::cout<<std::endl;

    std::sort(data.begin(),data.end(),Cmp_by_name());

    std::cout<<data;
    std::cout<<std::endl;

    std::sort(data.begin(),data.end(),Cmp_by_iid());

    std::cout<<data;
    std::cout<<std::endl;

    std::sort(data.begin(),data.end(),Cmp_by_value());

    std::cout<<data;
    std::cout<<std::endl;

    data.push_back(Item("k",99,12.34));
    data.push_back(Item("l",9988,499.95));

    std::cout<<data;
    std::cout<<std::endl;

    auto ptr = std::find(data.begin(),data.end(),Item("k"));
    data.erase(ptr);

    ptr = std::find(data.begin(),data.end(),Item("l"));
    data.erase(ptr);

    ptr = std::find(data.begin(),data.end(),Item("",0));
    data.erase(ptr);

    ptr = std::find(data.begin(),data.end(),Item("",1));
    data.erase(ptr);

    std::cout<<data;
    std::cout<<std::endl;

    std::list<Item> data_list;
    data_in(data_list);

    std::cout<<data_list;
    std::cout<<std::endl;

    data_list.sort(Cmp_by_name());

    std::cout<<data;
    std::cout<<std::endl;

    data_list.sort(Cmp_by_iid());

    std::cout<<data_list;
    std::cout<<std::endl;

    data_list.sort(Cmp_by_value());

    std::cout<<data_list;
    std::cout<<std::endl;

    data_list.push_back(Item("k",99,12.34));
    data_list.push_back(Item("l",9988,499.95));

    std::cout<<data_list;
    std::cout<<std::endl;

    auto ptr_1 = std::find(data_list.begin(),data_list.end(),Item("k"));
    data_list.erase(ptr_1);

    ptr_1 = std::find(data_list.begin(),data_list.end(),Item("l"));
    data_list.erase(ptr_1);

    ptr_1 = std::find(data_list.begin(),data_list.end(),Item("",0));
    data_list.erase(ptr_1);

    ptr_1 = std::find(data_list.begin(),data_list.end(),Item("",1));
    data_list.erase(ptr_1);

    std::cout<<data_list;
    std::cout<<std::endl;


    return 0;
}
