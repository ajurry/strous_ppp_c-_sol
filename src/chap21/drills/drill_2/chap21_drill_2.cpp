/*Basic use of maps*/

#include <iostream>
#include <sstream>
#include <string>
#include <map>

std::ostream& operator<<(std::ostream& ou, std::map<std::string,int>& data)
{
    for(auto ptr=data.begin(); ptr!=data.end(); ++ptr){
        ou<<ptr->first<<"\t"<<ptr->second<<"\n";
    }

    return ou;
}

std::ostream& operator<<(std::ostream& ou, std::map<int,std::string>& data)
{
    for(auto ptr=data.begin(); ptr!=data.end(); ++ptr){
        ou<<ptr->first<<"\t"<<ptr->second<<"\n";
    }

    return ou;
}


std::istream& operator>>(std::istream& in, std::map<std::string,int>& data)
{
    std::string temp;
    int val;

    std::cout<<"Please Input A String For Map"<<std::endl;
    in>>temp;
    std::cout<<"Please Input A Int For Map"<<std::endl;
    in>>val;
    data[temp]=val;
    return in;
}

void map_tot_erase (std::map<std::string,int>& data)
{
    for(auto ptr=data.begin(); ptr!=data.end(); ++ptr){
        data.erase(ptr);
    }
}

int map_sum_int (std::map<std::string,int>& data)
{
    int total=0;

    for(auto ptr=data.begin(); ptr!=data.end(); ++ptr){
        total += ptr->second;
    }

    return total;
}

void transfer (std::map<std::string,int>& data1, std::map<int,std::string>& data2)
{
    for(auto ptr=data1.begin(); ptr!=data1.end(); ++ptr){
        data2[ptr->second] = ptr->first;
    }
}

int main () {

    std::map<std::string,int> msi;

    int i;
    int sz=10;
    std::string var = "Var";
    std::stringstream temp;

    for(i=0;i<sz;i++){
        temp << var << i;
        msi[temp.str()]=i;
        temp.str("");
    }

    std::cout<<msi;
    map_tot_erase(msi);

    i=0;
    int sz_sm = 3;
    while(i<sz_sm){
    std::cin>>msi;
    ++i;
    }

    std::cout<<msi;
    std::cout<<std::endl;
    std::cout<<map_sum_int(msi)<<std::endl;

    std::map<int,std::string> mis;
    transfer(msi,mis);
    std::cout<<mis;

    return 0;
}
