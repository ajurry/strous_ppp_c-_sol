/*Basic manipulation of algorithms and sorting mechanisms with vectors*/

#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>

template<typename T>
T vec_sum (std::vector<T> temp){

    T total=0;
    for(auto str=temp.begin(); str!=temp.end(); ++str){
        total += *str;
    }
    return total;
}

class less_than {
    double val;
    double val2;
public:
    less_than(double vval, double vval2) : val(vval), val2(vval2) {}
    bool operator()(double val_cmp) const {return (val<val_cmp && val_cmp<val2);}
};

void data_in(std::vector<double>& data){

    std::ifstream ifs;
    ifs.open("input_chap21_3.txt");

    if(ifs.is_open()){
        std::cout<<"OPEN"<<std::endl;
    }
    else{
        std::cout<<"FILE NOT FOUND"<<std::endl;
    }

    double temp=0;
    while(ifs>>temp){
        data.push_back(temp);
    }

    ifs.close();
}

std::ostream& operator<<(std::ostream& ou, std::vector<double>& data){

    for(auto ptr = data.begin(); ptr!=data.end(); ++ptr){
        ou<<*ptr<<" ";
    }
    std::cout<<std::endl;

    return ou;
}

void vector_copy(std::vector<double>& data1, std::vector<int>& data2){

    while(data2.size() < data1.size()){
        data2.push_back(0);
    }

    std::cout<<data2.size()<<std::endl;
    copy(data1.begin(),data1.end(),data2.begin());
}

void vec_out_two(std::vector<double> data1, std::vector<int> data2){

    std::vector<double>::iterator dat_1;
    std::vector<int>::iterator dat_2 = data2.begin();

    for(dat_1=data1.begin(); dat_1!=data1.end(); ++dat_1){
        std::cout << *dat_1 << "\t" << *dat_2 <<std::endl;
        ++dat_2;
    }
}

int main () {

    std::vector<double> vd;
    std::vector<int> vi;

    data_in(vd);
    std::cout<<vd;
    std::cout<<std::endl;

    vector_copy(vd,vi);
    vec_out_two(vd,vi);

    double total_d = vec_sum(vd);
    int total_i = vec_sum(vi);

    std::cout<<std::endl;

    std::cout<<"TOTD: "<<"\t"<<total_d<<std::endl;
    std::cout<<"TOTI: "<<"\t"<<total_i<<std::endl;
    std::cout<<"DIFF: "<<"\t"<<(double)(total_d-total_i)<<std::endl;
    std::cout<<"MEAND: "<<"\t"<<(double)(total_d/vd.size())<<std::endl;

    double mean_d = (double)(total_d/vd.size());
    auto ptr_b = vd.begin();
    auto ptr_e = vd.end();

    std::reverse(ptr_b, ptr_e);

    std::cout<<std::endl;
    std::cout<<vd;
    std::cout<<std::endl;

    std::vector<double> vd2;

    int i;
    for(i=0;i<8;i++){
        vd2.push_back(0);
    }

    copy_if(vd.begin(),vd.end(),vd2.begin(),less_than(3.0,mean_d));
    std::cout<<vd2;
    std::cout<<std::endl;
    std::sort(vd.begin(),vd.end());
    std::cout<<vd;

    return 0;
}
