/*Experiments with tracing destuction and construction of elements
through a RAII style*/

#include "chap19_ex_4_1.h"
#include <iostream>

int count=1;

int main(){

    tracer<int> one;
    count++;
    tracer<int> two;
    count++;
    tracer<int> three;
    count++;
    tracer<int> four(three);
    three = one;

    return 0;
}
