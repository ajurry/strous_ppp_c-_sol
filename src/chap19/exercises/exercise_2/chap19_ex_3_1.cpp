/*Exercise for developing and using custom templates, one that resembles
std::vector and one that adds functionality to numbers, here a more complicated
version of custom vector is used*/


#include "chap19_ex_3_1.h"
#include "chap19_ex_3_2.h"
#include <iostream>

int main(){

    //vec play elements
    std::cout<<"vec Experimentation"<<std::endl;

    vec<int> vec_a;

    int i=0;
    for(i=0;i<3;i++){
        vec_a.push_back(20);
    }

    std::cout<<"print out"<<std::endl;
    std::cout<<"-------------------------------------------"<<std::endl;
    vec<int> vec_b = vec_a;
    vec_b = vec_a;
    for(i=0;i<(vec_a.size());i++){
        std::cout<<i+1<<'\t'<<vec_a[i]<<'\t'<<vec_b[i]<<std::endl;
    }

    vec_a.add_vec(vec_b);
    for(i=0;i<(vec_a.size());i++){
        std::cout<<i+1<<'\t'<<vec_a[i]<<'\t'<<vec_b[i]<<std::endl;
    }

    std::cout<<multi_vec(vec_a,vec_b)<<std::endl;
    std::cout<<"-------------------------------------------"<<std::endl;


    //number play elements
    std::cout<<'\n'<<std::endl;
    std::cout<<"num Experimentation"<<std::endl;

    number<int> num_a(5);
    number<int> num_b(20);
    number<double> num_c(50);


    std::cin>>num_a;
    num_a = num_b;

    std::cout<<std::endl;
    std::cout<<"print out"<<std::endl;
    std::cout<<"-------------------------------------------"<<std::endl;
    std::cout<<num_a<<std::endl;
    std::cout<<num_b<<std::endl;
    std::cout<<"-------------------------------------------"<<std::endl;

    //vec and number play elements
    std::cout<<'\n'<<std::endl;
    vec<number<int>> vecnum_a;
    vec<number<int>> vecnum_b;
    vecnum_a.push_back(num_a);
    vecnum_b.push_back(num_a);
    std::cout<<"print out"<<std::endl;
    std::cout<<"-------------------------------------------"<<std::endl;
    std::cout<<vecnum_a<<std::endl;
    std::cout<<multi_vec(vecnum_a,vecnum_b)<<std::endl;
    std::cout<<"-------------------------------------------"<<std::endl;
    std::cout<<std::endl;


    return 0;
}
