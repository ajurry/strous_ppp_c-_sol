/*More complex thorough manipulation of templates*/

#include "chap19_ex_1_1.h"

#include <iostream>
#include <string>
#include <vector>

int main(){

    test<int> var1(8);
    test<char> var2('A');
    test<double> var3(16);
    test<std::string> var4("ABC");
    test<std::vector<int>> var5(std::vector<int>(5,500));
    int i=0;

    std::cout<<"Reference Values"<<std::endl;
    std::cout<<var1.get_val()<<" ";
    std::cout<<var2.get_val()<<" ";
    std::cout<<var3.get_val()<<" ";
    std::cout<<var4.get_val()<<" ";

    for(i=0;i<int(var5.get_val().size());i++)
        std::cout<<var5.get_val()[i]<<" ";

    std::cout<<std::endl;
    std::cout<<"Set Values"<<std::endl;
    var1.set_val(0);
    std::cout<<var1.get_val()<<std::endl;

    std::cout<<"Overloaded Values"<<std::endl;
    var1=8;
    std::cout<<var1.get_val()<<std::endl;

    std::cout<<"Reading Values"<<std::endl;
    var1.read_val();
    var2.read_val();
    var3.read_val();
    var4.read_val();

    std::cout<<var1.get_val()<<" ";
    std::cout<<var2.get_val()<<" ";
    std::cout<<var3.get_val()<<" ";
    std::cout<<var4.get_val()<<" ";

    std::cout<<std::endl;

    return 0;
}
