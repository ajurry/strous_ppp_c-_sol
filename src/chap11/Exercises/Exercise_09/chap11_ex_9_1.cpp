/*Takes in a text file and converts it to ascii binary file, this binary
file is then taken in again and converted back to characters*/

#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>


int main (){

    std::ifstream ifs {"input.txt"};
    std::ofstream ofs {"output.txt",std::ios_base::binary};

    std::string word;
    std::stringstream data;

    std::vector<char> data_output;

    while(ifs.good()){
        getline(ifs,word);
        for(char& letter : word){
            data_output.push_back(letter);
        }
    }

    for(int i=0;i<int(data_output.size());i++){
        ofs << int(data_output[i]) << " ";
    }

    ofs.close();
    ifs.close();

    std::cout << "Converted" << std::endl;

    std::ifstream ifs2 {"output.txt",std::ios_base::binary};
    std::ofstream ofs2 {"output2.txt"};

    data_output.clear();

    std::cout << "Analysing" << std::endl;

    int a;
    std::vector<int> data_output2;

    while(ifs2>>a)
        data_output2.push_back(a);

    for(int a : data_output2){
        char c = a;
        std::cout<<c<<std::endl;
        ofs2 << c;
    }

    ifs2.close();
    ofs2.close();

}
