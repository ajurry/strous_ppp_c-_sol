/*Takes in numbers from the user and then converted into decimal*/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include <cmath>

void print(std::string type, std::string& original, double value);
void check(std::vector<std::string>& numbers);
double conversion(std::string& element, char type);
double table(char& value);


int main(){

    std::vector<std::string> numbers;
    std::string in;

    while(true){

    std::cout<<"Please Input Numbers, Using n to terminate"<<std::endl;
    std::cin>>in;

    if(in == "n" || in == "N"){
        break;
    }

    numbers.push_back(in);
    }

    check(numbers);

    return 0;
}

void print(std::string type, std::string& original, double value)
{
    std::cout << original << " " << type << " " << "converts to" << " "
        << value << " " <<"Decimal" <<std::endl;
}

void check(std::vector<std::string>& numbers)
{
    int i=0;
    int j=0;
    bool check1;
    double val=0;

    for(i=0;i<int(numbers.size());i++){

        check1 = false;

        for(j=0;j<int(numbers[i].length());j++){
            if(!isxdigit(numbers[i][j])){
                if(j==1){if(numbers[i][j] == 'x'){continue;}}
                else{
                std::cout << "INVALID" << " " << numbers[i] << std::endl;
                check1 = true;
                }
            }
        }

        if(check1){continue;}
        else if(int(numbers[i].length()) == 1){
            val = conversion(numbers[i],'d');
            print("Decimal",numbers[i],val);
        }
        else if(int(numbers[i].length()) == 2){
            if(numbers[i][0] == '0'){
            val = conversion(numbers[i],'o');
            print("Octal",numbers[i],val);
            }
            else{
            val = conversion(numbers[i],'d');
            std::cout<<"Value "<<val<<std::endl;
            print("Decimal",numbers[i],val);
            }
        }
        else{
            if(numbers[i][0] == '0' && numbers[i][1] == 'x'){
            val = conversion(numbers[i],'h');
            print("Hexidecimal",numbers[i],val);
            }
            else if(numbers[i][0] == '0'){
            val = conversion(numbers[i],'o');
            print("Octal",numbers[i],val);
            }
            else{
            val = conversion(numbers[i],'d');
            print("Decimal",numbers[i],val);
            }
        }
    }
}

double conversion(std::string& element, char type)
{
    int i=0;
    double total=0;
    double base=0;

    switch(type){
        case 'd':
            for(i=0;i<int(element.length());i++){
                base = pow(10,(int(element.length())-i-1));
                total += (table(element[i]) * base);
            }
            return total;
        case 'o':
            for(i=1;i<int(element.length());i++){
                base = pow(8,(int(element.length())-i-1));
                total += (table(element[i]) * base);
            }
            return total;
        case 'h':
            for(i=2;i<int(element.length());i++){
                base = pow(16,(int(element.length())-i-1));
                total += (table(element[i]) * base);
            }
            return total;

    }
    return total;
}


double table(char& value)
{
    switch(value){
        case '0': return 0;
        case '1': return 1;
        case '2': return 2;
        case '3': return 3;
        case '4': return 4;
        case '5': return 5;
        case '6': return 6;
        case '7': return 7;
        case '8': return 8;
        case '9': return 9;
        case 'A': return 10;
        case 'B': return 11;
        case 'C': return 12;
        case 'D': return 13;
        case 'E': return 14;
        case 'F': return 15;
    }

    return 100;
}
