/*Counts the occurances of a number in a text file, and then orders
in terms of size*/

#include <string>
#include <iostream>
#include <fstream>
#include <ios>
#include <iomanip>
#include <vector>
#include <algorithm>

struct Count{
    int var;
    int occurance;

    bool operator<( const Count& val ) const {
        return var < val.var;
    }
};



int main (){

    std::ifstream ifs {"input.txt",std::ios_base::in};

    std::vector<Count> data;

    int number;
    int i=0;
    bool ok;

    while(ifs>>number){

        for(i=0;i<int(data.size());i++){
            if(data[i].var == number){
                data[i].occurance += 1;
                ok = true;
                break;
            }
        }

        if(!ok){
            data.push_back(Count{number,1});
        }
        ok=false;
    }

    std::sort(data.begin(), data.end());

    for(i=0;i<int(data.size());i++){
        std::cout << "Number: " <<data[i].var << "\t\t" << "Count: "
            << data[i].occurance << std::endl;
    }

    return 0;
}
