/*Reads in a text file and writes out how many characters there
are of each character classification*/

#include <string>
#include <iostream>
#include <fstream>
#include <ios>
#include <vector>

int main (){

    std::ifstream ifs {"input.txt",std::ios_base::in};
    std::string in;

    std::vector<int> totals;

    std::cout << "Spc" << "\t" << "Alp" << "\t"
        << "Dec" << "\t" << "Up" << "\t" << "Low" << "\t" << "Pun"
        << "\t" << "Prt" << std::endl;

    int i=0;

    for(i=0;i<7;i++){
        totals.push_back(0);
    }

    while(ifs.good()){

        std::getline(ifs,in);

        for(char ch: in){

        if(std::isspace(ch)){totals[0] +=1 ;}
        if(std::isalpha(ch)){totals[1] +=1 ;}
        if(std::isdigit(ch)){totals[2] +=1 ;}
        if(std::isupper(ch)){totals[3] +=1 ;}
        if(std::islower(ch)){totals[4] +=1 ;}
        if(std::ispunct(ch)){totals[5] +=1 ;}
        if(std::isprint(ch)){totals[6] +=1 ;}
        }

    }

    std::cout << totals[0] << "\t" << totals[1] << "\t" << totals[2] << "\t"
        << totals[3] << "\t" << totals[4] << "\t" << totals[5] << "\t" << totals[6]
        <<  std::endl;

}
