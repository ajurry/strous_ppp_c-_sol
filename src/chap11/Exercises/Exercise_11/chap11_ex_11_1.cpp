/*Like in chap11_ex_11_1, but with the addition of adding extra
characters to each vector element*/

#include <vector>
#include <string>
#include <iostream>

std::vector<std::string> split(const std::string& s, const std::string& w);

int main(){

    std::string words;
    std::string extra;

    std::cout<<"Please Input Some Space Seperated Word"<<std::endl;
    std::getline(std::cin,words);
    std::cout<<"Please Input Some Word"<<std::endl;
    std::cin>>extra;


    std::vector<std::string> data = split(words,extra);

    int i = 0;


    for(i=0;i<int(data.size());i++){
        std::cout<<data[i]<<std::endl;
    }
}

std::vector<std::string> split(const std::string& s, const std::string& w){

    std::string store;
    std::vector<std::string> data;

    for(char letter : s){

        if(isspace(letter)){
            store += " ";
            store.append(w);
            data.push_back(store);
            store.clear();
        }
        else{
            store += letter;
        }
    }

    store += " ";
    store.append(w);
    data.push_back(store);
    store.clear();


    return data;
}
