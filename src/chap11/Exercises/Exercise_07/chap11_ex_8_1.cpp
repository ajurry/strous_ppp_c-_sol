/*Inputs text from a user and converts elements that are previously
contracted, here it only works on a few specific cases*/

#include <string>
#include <iostream>
#include <sstream>

void formal(std::string& in, int i);
void dash(std::string& in, int i);

int main (){

    int i=0;

    std::string in;
    bool check = true;

    std::cout<<"Please Input Strings To Read"<<std::endl;
    std::getline(std::cin,in);

    int size = int(in.length());

    for(i=0;i<size;i++){

        size = int(in.length());

        if(check){
            switch(in[i]){
                case '.': case ';': case ',': case '?':
                    in[i] = ' ';
            }
        }

        if (in[i] == '\''){
            formal(in,i);
        }

        if(in[i] == '-'){
            dash(in,i);
        }

        if(in[i] == '"' && check){
            check = false;
        }
        else if(in[i] == '"' && !check){
            check = true;
        }

    }

    std::cout << "\n" << in << "\t" << std::endl;
    return 0;
}

void formal(std::string& in, int i)
{
    if(i<int(in.length())-1 && i!=4){
        if(in[i+1] == 't' && in[i-1] == 'n' && in[i-3] == 'd'){
            in.insert(i+1," no");
            in.erase(i-1,2);
        }
        else if(in[i+1] == 't' && in[i-1] == 'n' && in[i-3] == 'c'){
            in[i] = ' ';
            in.insert(i+1,"no");
        }
        else{
            in[i] = ' ';
        }
    }
}

void dash(std::string& in, int i)
{
    if(i<int(in.length())-1 && i!=0){
        if(!(isgraph(in[i-1]) && isgraph(in[i+1]))){
            in[i] = ' ';
        }
    }
}
