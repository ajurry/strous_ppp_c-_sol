/*Takes in input from a text file and stores it in a string,
it then reverses the order of the input*/

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <ios>

int main(){

    std::string out;
    std::string buffer;
    std::string in;
    std::ifstream ifs {"input.txt",std::ios_base::in};

    std::getline(ifs,in);

    for(char& letter : in){
        if(isspace(letter)){
            out.insert(0," " + buffer);
            buffer.clear();
        }
        else{
            buffer += letter;
        }
    }

    out.insert(0,buffer);

    std::cout<<in<<std::endl;
    std::cout<<out<<std::endl;

    return 0;
}
