/*Checks string input from the user and then replaces or grammer with spaces*/

#include <string>
#include <iostream>

int main (){

    std::string in;
    bool check = true;

    std::cout<<"Please Input Strings To Read"<<std::endl;
    std::getline(std::cin,in);

    std::cout << "\n" << in << std::endl;

    for(char& ch: in){

        if(check){
            switch(ch){
                case '.': case ';': case ',': case '?': case '-': case '\'':
                ch = ' ';
            }
        }

        if(ch == '"' && check){
            check = false;
        }
        else if(ch == '"' && !check){
            check = true;
        }
    }

    std::cout << in << std::endl;
}
