/*Reads in text from a text file and converts all the capital letter
into a lower case which is then stored into another text file*/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>

int main(){

    std::ifstream ifs{"Input.txt"};
    std::ofstream ofs{"Output.txt"};

    std::string Words;

    while(ifs>>Words){
        for(char& ch : Words){ch = std::tolower(ch);}
        ofs<<Words<<" " ;
    }

    ifs.close();
    ofs.close();

}
