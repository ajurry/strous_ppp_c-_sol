/*Reverses a text file, using an insert on a string and also a deque*/

#include <deque>
#include <string>
#include <iostream>
#include <fstream>
#include <ios>

int main(){

    std::string output;
    std::string buffer;
    std::ifstream ifs {"input.txt",std::ios_base::in};

    ifs >> buffer;

    for(char& letter : buffer){
        output.insert (0,1,letter);
    }

    std::deque<char> reverse;
    for(char& letter : buffer){
        reverse.push_front(letter);
    }

    for(auto a=reverse.begin(); a<reverse.end(); ++a){
        std::cout<<*a;
    }
    std::cout<<std::endl;
    std::cout<<output<<std::endl;

    return 0;
}
