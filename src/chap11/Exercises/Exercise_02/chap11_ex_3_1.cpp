/*This program takes a users input for a word and then scans a text file,
the lines that contain these words are then stored into a seperate text file*/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>

int main(){

    std::ifstream ifs{"Input.txt"};
    std::ofstream ofs{"Output.txt"};

    std::string Check;
    std::string Raw;

    std::stringstream Words;

    std::cout<<"Please input a word for search criteria"<<std::endl;
    std::cin >> Check;

    while(!ifs.eof()){

        std::getline(ifs,Raw);
        Words << Raw;

        for(std::string Word; Words>>Word;){

            if (Word == Check){
                ofs << Raw << std::endl;
            }
        }

        Words.clear();
        //This is required as the stringsteam hit EOF() on the line,
        //this means it enters a bad state not allowing anymore input
        //until set correctly

        Words.str(std::string());
    }

    ifs.close();
    ofs.close();

}
