/*Program that splits a string that is space seperated into a vector*/

#include <vector>
#include <string>
#include <iostream>

std::vector<std::string> split(const std::string& s);

int main(){

    std::string words;

    std::cout<<"Please Input Some Space Seperated Word"<<std::endl;
    std::getline(std::cin,words);

    std::vector<std::string> data = split(words);

    int i = 0;


    for(i=0;i<int(data.size());i++){
        std::cout<<data[i]<<std::endl;
    }
}

std::vector<std::string> split(const std::string& s){

    std::string store;
    std::vector<std::string> data;

    for(char letter : s){

        if(isspace(letter)){
            data.push_back(store);
            store.clear();
        }
        else{
            store += letter;
        }
    }

    data.push_back(store);
    store.clear();


    return data;
}
