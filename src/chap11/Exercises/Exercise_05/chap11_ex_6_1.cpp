/*This takes in a string from a user and then checks each character of type*/

#include <string>
#include <iostream>

int main (){

    std::string in;

    std::cout<<"Please Input Strings To Read"<<std::endl;
    std::getline(std::cin,in);

    std::cout << "Cha" << "\t" << "Spc" << "\t" << "Alp" << "\t"
        << "Dec" << "\t" << "Up" << "\t" << "Low" << "\t" << "Pun"
        << "\t" << "Prt" << std::endl;

    for(char ch: in){

    std::cout << ch << "\t";
    if(std::isspace(ch)){std::cout << "1" << "\t";}
    else{std::cout << "0" << "\t";}
    if(std::isalpha(ch)){std::cout << "1" << "\t";}
    else{std::cout << "0" << "\t";}
    if(std::isdigit(ch)){std::cout << "1" << "\t";}
    else{std::cout << "0" << "\t";}
    if(std::isupper(ch)){std::cout << "1" << "\t";}
    else{std::cout << "0" << "\t";}
    if(std::islower(ch)){std::cout << "1" << "\t";}
    else{std::cout << "0" << "\t";}
    if(std::ispunct(ch)){std::cout << "1" << "\t";}
    else{std::cout << "0" << "\t";}
    if(std::isprint(ch)){std::cout << "1" << "\t";}
    else{std::cout << "0" << "\t";}
    std::cout << "\n" << std::endl;
    }
}
