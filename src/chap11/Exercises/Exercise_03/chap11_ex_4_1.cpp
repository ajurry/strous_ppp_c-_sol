/*Takes a file input and then takes out all the vowels and then places all
the words into a seperate text file*/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>

void take_vowels_out(std::string& Word);

int main(){

    std::ifstream ifs{"Input.txt"};
    std::ofstream ofs{"Output.txt"};

    std::string Raw;

    std::stringstream Words;

    while(!ifs.eof()){

        std::getline(ifs,Raw);
        Words << Raw;

        for(std::string Word; Words>>Word;){

            take_vowels_out(Word);

            ofs << Word << std::endl;
        }

        Words.clear();

        Words.str(std::string());
    }

    ifs.close();
    ofs.close();

}

void take_vowels_out(std::string& Word)
{
    std::string Check = "AOUIEaouie";

    std::cout<<Word<<"\t";

    for(int i=0; i<int(Word.length()); i++){
        for(char Letter : Check){
            if(Word[i]==Letter){
                std::cout<<Word[i]<<"\t";
                Word.erase(Word.begin() + i);
            }
        }
    }
    std::cout<<std::endl;
}
