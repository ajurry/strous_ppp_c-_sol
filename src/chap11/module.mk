CHAP_DIR:= $(notdir $(abspath $(dir $(lastword $(MAKEFILE_LIST)))))


QUESTION_1_SRCS := ${CHAP_DIR}/Drill/chap11_ex_1_1.cpp
QUESTION_1_OBJS := ${QUESTION_1_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap11_prog_1: ${QUESTION_1_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_2_SRCS := ${CHAP_DIR}/Exercises/Exercise_01/chap11_ex_2_1.cpp
QUESTION_2_OBJS := ${QUESTION_2_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap11_prog_2: ${QUESTION_2_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_3_SRCS := ${CHAP_DIR}/Exercises/Exercise_02/chap11_ex_3_1.cpp
QUESTION_3_OBJS := ${QUESTION_3_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap11_prog_3: ${QUESTION_3_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_4_SRCS := ${CHAP_DIR}/Exercises/Exercise_03/chap11_ex_4_1.cpp
QUESTION_4_OBJS := ${QUESTION_4_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap11_prog_4: ${QUESTION_4_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_5_SRCS := ${CHAP_DIR}/Exercises/Exercise_04/chap11_ex_5_1.cpp
QUESTION_5_OBJS := ${QUESTION_5_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap11_prog_5: ${QUESTION_5_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_6_SRCS := ${CHAP_DIR}/Exercises/Exercise_05/chap11_ex_6_1.cpp
QUESTION_6_OBJS := ${QUESTION_6_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap11_prog_6: ${QUESTION_6_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_7_SRCS := ${CHAP_DIR}/Exercises/Exercise_06/chap11_ex_7_1.cpp
QUESTION_7_OBJS := ${QUESTION_7_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap11_prog_7: ${QUESTION_7_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_8_SRCS := ${CHAP_DIR}/Exercises/Exercise_07/chap11_ex_8_1.cpp
QUESTION_8_OBJS := ${QUESTION_8_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap11_prog_8: ${QUESTION_8_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_9_SRCS := ${CHAP_DIR}/Exercises/Exercise_09/chap11_ex_9_1.cpp
QUESTION_9_OBJS := ${QUESTION_9_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap11_prog_9: ${QUESTION_9_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_10_SRCS := ${CHAP_DIR}/Exercises/Exercise_10/chap11_ex_10_1.cpp
QUESTION_10_OBJS := ${QUESTION_10_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap11_prog_10: ${QUESTION_10_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_11_SRCS := ${CHAP_DIR}/Exercises/Exercise_11/chap11_ex_11_1.cpp
QUESTION_11_OBJS := ${QUESTION_11_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap11_prog_11: ${QUESTION_11_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_12_SRCS := ${CHAP_DIR}/Exercises/Exercise_12/chap11_ex_12_1.cpp
QUESTION_12_OBJS := ${QUESTION_12_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap11_prog_12: ${QUESTION_12_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_13_SRCS := ${CHAP_DIR}/Exercises/Exercise_13/chap11_ex_13_1.cpp
QUESTION_13_OBJS := ${QUESTION_13_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap11_prog_13: ${QUESTION_13_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_14_SRCS := ${CHAP_DIR}/Exercises/Exercise_14/chap11_ex_14_1.cpp
QUESTION_14_OBJS := ${QUESTION_14_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap11_prog_14: ${QUESTION_14_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_15_SRCS := ${CHAP_DIR}/Exercises/Exercise_16/chap11_ex_15_1.cpp
QUESTION_15_OBJS := ${QUESTION_15_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap11_prog_15: ${QUESTION_15_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

TARGETS+=${BINDIR}/chap11_prog_1 \
		 ${BINDIR}/chap11_prog_2 \
		 ${BINDIR}/chap11_prog_3 \
		 ${BINDIR}/chap11_prog_4 \
		 ${BINDIR}/chap11_prog_5 \
		 ${BINDIR}/chap11_prog_6 \
		 ${BINDIR}/chap11_prog_7 \
		 ${BINDIR}/chap11_prog_8 \
		 ${BINDIR}/chap11_prog_9 \
		 ${BINDIR}/chap11_prog_10 \
		 ${BINDIR}/chap11_prog_11 \
		 ${BINDIR}/chap11_prog_12 \
		 ${BINDIR}/chap11_prog_13 \
		 ${BINDIR}/chap11_prog_14 \
		 ${BINDIR}/chap11_prog_15

