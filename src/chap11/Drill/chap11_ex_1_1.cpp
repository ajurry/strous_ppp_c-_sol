/*Basic data manipulation that changes data types, by changing
base and by changing number type display*/

#include <iostream>
#include <string>
#include <iomanip>

int main(){

    int birth_year=1993;

    std::cout << std::showbase
        << "OCT" << "\t" << std::oct << birth_year << "\n"
        << "DEC" << "\t" << std::dec << birth_year << "\n"
        << "HEX" << "\t" << std::hex << birth_year << "\n"
        << std::endl;

    int a=0;
    int b=0;
    int c=0;
    int d=0;

    std::cin >> std::dec >> a >> std::oct >> b >> std::hex >> c >> d;

    std::cout << std::endl;

    std::cout << std::showbase
        << "OCT" << "\t" << std::oct << a << "\n"
        << "DEC" << "\t" << std::dec << b << "\n"
        << "HEX" << "\t" << std::hex << c << "\n"
        << "HEX" << "\t" << std::hex << d << "\n"
        << std::endl;

    double number = 1234567.89;

    std::cout << std::endl;

    std::cout << "FloatNum" << "\t" << std::defaultfloat << std::setw(7)
        << std::setprecision(10) << number << "\n"
            << "FixedNum" << "\t" << std::fixed << std::setw(7)
         << std::setprecision(2) << number << "\n"
            << "ScienNum" << "\t" << std::scientific << std::setw(7)
         << std::setprecision(8) << number << "\n" << std::endl;

    return 0;
}
