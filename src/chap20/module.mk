CHAP_DIR:= $(notdir $(abspath $(dir $(lastword $(MAKEFILE_LIST)))))


QUESTION_1_SRCS := ${CHAP_DIR}/test/test_1/chap20_test_1.cpp
QUESTION_1_OBJS := ${QUESTION_1_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap20_prog_1: ${QUESTION_1_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


QUESTION_2_SRCS := ${CHAP_DIR}/test/test_2/chap20_test_2.cpp
QUESTION_2_OBJS := ${QUESTION_2_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap20_prog_2: ${QUESTION_2_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


QUESTION_3_SRCS := ${CHAP_DIR}/drills/drill_1/chap20_ex_1_1.cpp
QUESTION_3_OBJS := ${QUESTION_3_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap20_prog_3: ${QUESTION_3_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_4_SRCS := ${CHAP_DIR}/exercises/exercise_2/chap20_ex_3_1.cpp
QUESTION_4_OBJS := ${QUESTION_4_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap20_prog_4: ${QUESTION_4_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


TARGETS+=${BINDIR}/chap20_prog_1 \
		 ${BINDIR}/chap20_prog_2 \
		 ${BINDIR}/chap20_prog_3 \
		 ${BINDIR}/chap20_prog_4
