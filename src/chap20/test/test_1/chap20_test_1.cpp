/*Basic copy test using iterators*/

#include <iostream>
#include <string>

void copy(int* f1, int* e1, int* f2);

int main() {

    int sz = 20;
    int i = 0;

    int* array1 = new int[sz];
    int* array2 = new int[sz];

    for(i=0;i<sz;i++){
        array1[i] = i;
        array2[i] = i;
    }

    copy(&array1[0], &array1[sz], &array2[0]);

    for(i=0;i<sz;i++){
        std::cout<<"array1["<<i<<"] "<<array1[i]<<"\t"
            <<"array2["<<i<<"] "<<array2[i]<<std::endl;
    }

    delete[] array1;
    delete[] array2;
    return 0;
}

void copy(int* f1, int* e1, int* f2)
{

    int* ptr;

    for(ptr=f1;ptr!=e1;ptr++){
        f2 = ptr;
        f2++;
    }

}
