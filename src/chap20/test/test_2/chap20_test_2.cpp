/*Further experimentation with iterators, using different STL containers*/

#include <iostream>
#include <list>
#include <vector>
#include <string>

int main() {

    //Storage Options
    std::string string1;
    std::vector<char> vector1;
    std::list<char> list1;

    //String Initialisation
    string1 = "Hello";

    //Vector Initialisation
    vector1.push_back('H');
    vector1.push_back('e');
    vector1.push_back('l');
    vector1.push_back('l');
    vector1.push_back('o');

    //List Initialisation
    list1.push_back('H');
    list1.push_back('e');
    list1.push_back('l');
    list1.push_back('l');
    list1.push_back('o');

    //Print Out
    for(std::vector<char>::iterator ve = vector1.begin();
            ve!=vector1.end(); ve++)
    {
        std::cout<<"vec "<<*ve<<std::endl;
    }

    for(std::list<char>::iterator le = list1.begin();
            le!=list1.end(); le++)
    {
        std::cout<<"list "<<*le<<std::endl;
    }

    return 0;
}
