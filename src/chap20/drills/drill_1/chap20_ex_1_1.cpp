/*More advanced iterator use, using iterators from three different
containers and applying data manipulation*/

#include <vector>
#include <array>
#include <list>
#include <iostream>
#include <algorithm>

template<typename Iter1, typename Iter2>
void copy_iter(Iter1 f1, Iter1 e1, Iter2 f2)
{
    Iter2 q = f2;
    for(Iter1 p = f1; p!=e1; p++)
    {
        *q=*p;
        q++;
    }
}

int main () {

    std::array<int, 10> array_1 {0,1,2,3,4,5,6,7,8,9};
    std::array<int, 10>::iterator a;
    //Iterator which is a class inside array

    std::vector<int> vec_1;
    std::list<int> list_1;

    //Setting the values of vector
    for(a=array_1.begin(); a!=array_1.end(); a++)
    {
        vec_1.push_back(*a);
        list_1.push_back(*a);
    }

    //Copying to another of the same type
    std::array<int, 10> array_2 {array_1};
    std::vector<int> vec_2 {vec_1};
    std::list<int> list_2 {list_1};

    //Add extra to the data structures
    std::vector<int>::iterator v;
    std::list<int>::iterator l;


    for(a=array_1.begin(); a!=array_1.end(); a++)
    {
        *a += 2;
    }

    for(v=vec_1.begin(); v!=vec_1.end(); v++)
    {
        *v += 3;
    }

    for(l=list_1.begin(); l!=list_1.end(); l++)
    {
        *l += 5;
    }

    std::cout<<"==========================================="<<std::endl;

    v=vec_1.begin();
    l=list_1.begin();
    for(a=array_1.begin(); a!=array_1.end(); a++)
    {
        std::cout<<"Array: "<<*a<<"\t" <<"Vector: "<<*v<<"\t"
            <<"List: "<<*l<<std::endl;
        v++;
        l++;
    }

    std::cout<<"-------------------------------------------"<<std::endl;

    v=vec_2.begin();
    l=list_2.begin();
    for(a=array_2.begin(); a!=array_2.end(); a++)
    {
        std::cout<<"Array: "<<*a<<"\t" <<"Vector: "<<*v<<"\t"
            <<"List: "<<*l<<std::endl;
        v++;
        l++;
    }

    std::cout<<"==========================================="<<std::endl;

    copy_iter(array_1.begin(),array_1.end(),array_2.begin());
    copy_iter(array_1.begin(),array_1.end(),vec_2.begin());
    copy_iter(array_1.begin(),array_1.end(),list_2.begin());

    v=vec_1.begin();
    l=list_1.begin();
    for(a=array_1.begin(); a!=array_1.end(); a++)
    {
        std::cout<<"Array: "<<*a<<"\t" <<"Vector: "<<*v<<"\t"
            <<"List: "<<*l<<std::endl;
        v++;
        l++;
    }

    std::cout<<"-------------------------------------------"<<std::endl;

    v=vec_2.begin();
    l=list_2.begin();
    for(a=array_2.begin(); a!=array_2.end(); a++)
    {
        std::cout<<"Array: "<<*a<<"\t" <<"Vector: "<<*v<<"\t"
            <<"List: "<<*l<<std::endl;
        v++;
        l++;
    }


    std::cout<<"==========================================="<<std::endl;

    v = std::find(vec_1.begin(), vec_1.end(), 5);

    int i=0;
    for(auto p=vec_1.begin(); p!=vec_1.end();++p)
    {
        if(p==v)
            break;
        else
            i++;
    }

    if(v != vec_1.end())
        std::cout << "Element Found In Vector: "<<*v<<" at "<<i
            <<std::endl;
    else
        std::cout<< "Element Not Found In Vector"<<std::endl;

    return 0;
}
