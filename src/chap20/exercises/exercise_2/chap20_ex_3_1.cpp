/*Time comparison between vector and list, both with sorted elements, number of
elements to insert is given via user input*/

#include <list>
#include <vector>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <ctime>
#include <stdlib.h>

int main (int argc, char* argv[]) {

    int i;
    int limit = atoi (argv[1]);
    std::list<int> list_a;
    std::vector<int> vec_a;

    std::clock_t t_f;

    srand(time(NULL));

    std::clock_t t_s = std::clock();
    int rngNumber = (rand() % 100 + 1);
    vec_a.push_back(rngNumber);

    //Need to rng a number
    //Decide if it is bigger or smaller than the previous number
    //Move pointer down one level
    //Insert number

    std::vector<int>::iterator oneBeforeLastVec;


    for(i=0;i<limit;i++){
        oneBeforeLastVec = vec_a.end();
        --oneBeforeLastVec;

        rngNumber = (rand() % 100 + 1);

        for(auto iter=vec_a.begin(); iter!=vec_a.end(); ++iter)
        {
            if(rngNumber<=*iter){
                vec_a.insert(iter,rngNumber);
                break;
            }
            if(iter==oneBeforeLastVec){
                vec_a.insert(vec_a.end(),rngNumber);
                break;
            }
        }
    }

    t_f = std::clock();
    std::cout<<"TIME VEC: "<<(double)(t_f-t_s)<<std::endl;





    t_s = std::clock();
    rngNumber = (rand() % 100 + 1);
    list_a.push_back(rngNumber);

    std::list<int>::iterator oneBeforeLastList;

    for(i=0;i<limit;i++){
        oneBeforeLastList = list_a.end();
        --oneBeforeLastList;
        rngNumber = (rand() % 100 + 1);

        for(auto iter=list_a.begin(); iter!=list_a.end(); ++iter)
        {
            if(rngNumber<=*iter){
                list_a.insert(iter,rngNumber);
                break;
            }
            if(iter==oneBeforeLastList){
                list_a.insert(list_a.end(),rngNumber);
                break;
            }
        }
    }

    t_f = std::clock();
    std::cout<<"TIME LIST: "<<(double)(t_f-t_s)<<std::endl;



    return 0;
}
