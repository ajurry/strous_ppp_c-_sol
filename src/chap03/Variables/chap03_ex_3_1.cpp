/*Basic program for input, demonstrating what happens upon character input
for an integer*/

#include <iostream>
#include <string>

int main()
{
	std::cout <<"Please Enter Your First Name And Age\n";

	std::string first_name = "???"; //String Variable

	int age {-1}; //Integer Variable

	std::cin >>first_name>>age; //Read a string and then read an integer

	std::cout <<"Hello, "<<first_name<<" (age "<<age<<")\n";
}
