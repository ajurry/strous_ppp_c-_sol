/*Basic input output for C++ */

#include <iostream>
#include <string>

int main()
{
	std::cout << "Please Input Your First Name (Followed By 'Enter'):\n";
	std::string first_name;
	std::cin >> first_name;
	std::cout << "Hello, "<<first_name<<"!\n";
}
