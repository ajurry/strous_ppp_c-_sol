CHAP_DIR := $(notdir $(abspath $(dir $(lastword $(MAKEFILE_LIST)))))


INPUT_SRCS := ${CHAP_DIR}/Input/chap03_ex_1_1.cpp
INPUT_OBJS := ${INPUT_SRCS:%.cpp=${OBJDIR}/%.o}

${BINDIR}/chap03_prog_1: ${INPUT_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${CXX} ${LXXFLAGS} $< -o $@


OPERATORS_1_SRCS := ${CHAP_DIR}/Operators/chap03_ex_2_1.cpp
OPERATORS_1_OBJS := ${OPERATORS_1_SRCS:%.cpp=${OBJDIR}/%.o}

${BINDIR}/chap03_prog_2: ${OPERATORS_1_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${CXX} ${LXXFLAGS} $< -o $@


OPERATORS_2_SRCS := ${CHAP_DIR}/Operators/chap03_ex_2_2.cpp
OPERATORS_2_OBJS := ${OPERATORS_2_SRCS:%.cpp=${OBJDIR}/%.o}

${BINDIR}/chap03_prog_3: ${OPERATORS_2_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${CXX} ${LXXFLAGS} $< -o $@


VARIABLES_SRCS := ${CHAP_DIR}/Variables/chap03_ex_3_1.cpp
VARIABLES_OBJS := ${VARIABLES_SRCS:%.cpp=${OBJDIR}/%.o}

${BINDIR}/chap03_prog_4: ${VARIABLES_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${CXX} ${LXXFLAGS} $< -o $@

TARGETS+=${BINDIR}/chap03_prog_1 \
		 ${BINDIR}/chap03_prog_2 \
		 ${BINDIR}/chap03_prog_3 \
		 ${BINDIR}/chap03_prog_4



