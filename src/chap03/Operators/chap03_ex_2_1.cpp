/*Prints out all ascii characters and their assiociated number*/

#include <iostream>
#include <string>

int main()
{
    std::cout << "Type Yes To Start\n";
    std::string start;

    std::cin >> start;

    while(start != "Yes"){
        std::cout << "Type Yes To Start\n";
        std::cin >> start;
    }

    int i=0;
    int check=0;
    char character;

    for(i=0;i<500;i++){
        character = i;
        check = character;

        if(check != i)
            break;
        else
        std::cout << character << '\t' << i << '\n';

    }
}
