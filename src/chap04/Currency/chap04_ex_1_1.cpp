/*Basic program dealing with a switch statement that corresponds
to a currency conversion*/

#include <iostream>
#include <string>

int main()
{

    const double dy = 120.17;
    const double pd = 1.51;
    const double py = 181.92;

    double money;
    char currency;
    char conversion;

    std::cout<<"Input Amount Of Money,"<<'\n'<<"Input Currency Type, and"
    <<'\n'<<"Currency Conversion Type"<<
    '\n'<<"Pound (P), Dollar (D), Yen (Y)"<<'\n'
    <<"Case Sensitive"<<'\n'
    <<"Example: 565 D Y"<<'\n';

    std::cin>>money>>currency>>conversion;

    switch (currency){
    case 'P': switch(conversion){
                case 'D': money *= pd; break;
                case 'Y': money *= py; break;
                case 'P': break;
              }

                std::cout<<money<<conversion<<'\n';
                          break;

    case 'D': switch(conversion){
                case 'P': money /= pd; break;
                case 'Y': money *= dy; break;
                case 'D': break;
              }

                std::cout<<money<<conversion<<'\n';
                          break;

    case 'Y': switch(conversion){
                case 'Y': break;
                case 'P': money /= py; break;
                case 'D': money /= dy; break;

                std::cout<<money<<conversion<<'\n';
                break;
              }

    default:
        std::cout<<"The entered option is not a valid option\n"; break;
    }
}
