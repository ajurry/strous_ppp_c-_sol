CHAP_DIR := $(notdir $(abspath $(dir $(lastword $(MAKEFILE_LIST)))))


CURRENCY_SRCS := ${CHAP_DIR}/Currency/chap04_ex_1_1.cpp
CURRENCY_OBJS := ${CURRENCY_SRCS:%.cpp=${OBJDIR}/%.o}

${BINDIR}/chap04_prog_1: ${CURRENCY_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${CXX} ${LXXFLAGS} $< -o $@


VECTORS_1_SRCS := ${CHAP_DIR}/Vectors/chap04_ex_2_1.cpp
VECTORS_1_OBJS := ${VECTORS_1_SRCS:%.cpp=${OBJDIR}/%.o}

${BINDIR}/chap04_prog_2: ${VECTORS_1_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${CXX} ${LXXFLAGS} $< -o $@


VECTORS_2_SRCS := ${CHAP_DIR}/Vectors/chap04_ex_2_2.cpp
VECTORS_2_OBJS := ${VECTORS_2_SRCS:%.cpp=${OBJDIR}/%.o}

${BINDIR}/chap04_prog_3: ${VECTORS_2_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${CXX} ${LXXFLAGS} $< -o $@


VECTORS_3_SRCS := ${CHAP_DIR}/Vectors/chap04_ex_2_3.cpp
VECTORS_3_OBJS := ${VECTORS_3_SRCS:%.cpp=${OBJDIR}/%.o}

${BINDIR}/chap04_prog_4: ${VECTORS_3_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${CXX} ${LXXFLAGS} $< -o $@


TARGETS+=${BINDIR}/chap04_prog_1 \
		 ${BINDIR}/chap04_prog_2 \
		 ${BINDIR}/chap04_prog_3 \
		 ${BINDIR}/chap04_prog_4



