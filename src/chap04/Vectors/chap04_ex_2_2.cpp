/*Same as before, except adding to the original vector and passing vectors
to other vectors*/

#include <iostream>
#include <string>
#include <vector>

int main ()
{

    std::vector<int> val = {1,2,3,4,5,6};
    std::vector<std::string> lang = {"Python","C","C++"};

    std::vector<int> unval(6);
    std::vector<std::string> unlang(3);
    unlang = lang;

    int i=0;

    for(i=0;i<int(lang.size());++i){
        std::cout<<"Term"<<i<<'\t'<<unlang[i]<<'\n';
    }

    lang.push_back("Fortran");

    std::cout<<'\n';

    for(i=0;i<int(lang.size());++i){
        std::cout<<"Term"<<i<<'\t'<<lang[i]<<'\n';
    }


}
