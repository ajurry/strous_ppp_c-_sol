/*Basic vector program that looks through a vector and applies logic to certain
words*/

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

int main ()
{

    std::vector<std::string> dict;
    std::string temp;
    std::string naughty="word";
    int i=0;

    std::cout<<"Input words to fill the vector, terminating input upon /"
    <<std::endl;

    while(std::cin>>temp){
        if (temp=="/")
        {
            break;
        }
        dict.push_back(temp);
    }
    std::cout <<"Number Of Words:"<<dict.size()<<'\n';

    for(i=0;i<int(dict.size());++i){
        if(dict[i] != naughty)
            std::cout << dict[i] <<' ';
        else
            std::cout << "****" <<' ';

        std::cout<<'\n';
    }
}
