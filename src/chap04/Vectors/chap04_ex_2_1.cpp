/*Program that initializes a vector and then prints its contents*/

#include <iostream>
#include <string>
#include <vector>

int main ()
{

    std::vector<int> val = {1,2,3,4,5,6};
    std::vector<std::string> lang = {"Python","C","C++"};

    int i=0;

    for(i=0;i<int(lang.size());++i){
        std::cout<<"Term"<<i<<'\t'<<lang[i]<<'\n';
    }

}
