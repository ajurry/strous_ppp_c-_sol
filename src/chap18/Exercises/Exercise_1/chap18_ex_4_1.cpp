/*Exercise in manipulating C-style arrays and the use of C print functions*/

#include <cstdio>
#include <cstdlib>

char* strdup(const char* words);
char* findx(const char* s, const char* x);
char* cat_dot(const char* s, const char* x);
int strcmp(const char* s1, const char* s2);
int size(const char* s);

int main() {

    char words[] = "Test example to check program: example";
    char* array = strdup(words);

    char fnd[] = "example";
    char* found = findx(array, fnd);

    int lex = strcmp(found,array);

    char* con = cat_dot(array,found);

    printf("Words: %s\n",array);
    printf("Found: %s\n",found);
    printf("Lexi:  %d\n",lex);
    printf("Con:   %s\n",con);

    free(array);
    free(found);
    delete[] con;
    return 0;
}

char* strdup(const char* words)
{
    int count=size(words);

    char* array = (char*)malloc((count+1)*sizeof(char));
    count=0;

    while(words[count] != '\0'){
        array[count] = words[count];
        count++;
    }

    array[count+1] = '\0';

    return array;
}

char* findx(const char* s, const char* x)
{
    int count=0;
    int checker=0;
    int i=0;
    bool fin=false;

    while(s[count] != '\0'){
        count ++;

        while(s[count+checker] == x[checker]){
            checker++;
            if(x[checker+1] == '\0'){
                fin = true;
                break;
            }
        }

        checker=0;
        if(fin)
            break;
    }

    while(s[count + checker] != '\0'){
        checker++;
    }

    char* r = (char*)malloc((checker+1)*sizeof(char));

    for(i=0;i<checker;i++){
        r[i] = s[i+count];
    }

    r[i+1] = '\0';

    return r;
}

char* cat_dot(const char* s, const char* x)
{
    int sizes=size(s);
    int sizex=size(x);
    int i=0;

    char* con = new char[sizes+sizex+2];

    for(i=0;i<sizes;i++){
        con[i] = s[i];
    }
    con[i] = '.';
    for(i=sizes+1;i<sizex+1+sizes;i++){
        con[i] = x[i-sizes-1];
    }
    printf("\n");

    con[i+1] = '\0';
    return con;
}

int strcmp(const char* s1, const char* s2)
{

    int size1=size(s1);
    int size2=size(s2);
    int i=0;

    while(s1[i] != '\0' || s2[i] != '\0'){

        if(s1[i] < s2[i]){return -1;}
        else if(s2[i] < s1[i]){return 1;}
    }

    if(size1 == size2){return 0;}
    else if(size1 < size2){return -1;}
    else if(size2 < size1){return 1;}

    return 2;
}

int size(const char* s)
{
    int i=0;

    while(s[i] != '\0'){
        i++;
    }

    return i;
}
