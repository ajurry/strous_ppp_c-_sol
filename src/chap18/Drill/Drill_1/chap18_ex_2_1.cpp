/*Drill revolving around the use of arrays, which is then repeated but
for vectors, to show the improvement*/

#include <iostream>
#include <string>

int ga[10] = {1,2,4,8,16,32,64,128,256,512};

void array_manip(int array[10],int size);

int main() {

    std::cout<<"Running"<<std::endl;

    array_manip(ga,10);

    int aa[10] = {1,2,6,24,120,720,5040,40320,362880,3628800};

    array_manip(aa,10);

    return 0;
}

void array_manip(int array[10], int size)
{

    int la[10];
    int i=0;

    for(i=0;i<10;i++){
        la[i] = array[i];
    }

    for(i=0;i<10;i++){
        std::cout<<"la:"<<la[i]<<"\t"<<"a:"<<array[i]<<std::endl;
    }

    int* ptr = new int[10];

    for(i=0;i<10;i++){
        ptr[i] = array[i];
    }

    for(i=0;i<10;i++){
        std::cout<<"fsa:"<<ptr[i]<<"\t"<<"a:"<<array[i]<<std::endl;
    }
    //Here fsa corresponds to the free store array

    delete[] ptr;
}
