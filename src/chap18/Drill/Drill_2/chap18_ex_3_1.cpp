/*Drill revolving around the use of vectors, showing their benefits*/

#include <iostream>
#include <string>
#include <vector>

std::vector<int> gv = {1,2,4,8,16,32,64,128,256,512};

void vector_manip(std::vector<int> vec);

int main () {

    vector_manip(gv);

    std::vector<int> vv = {1,2,6,24,120,720,5040,40320,362880,3628800};

    vector_manip(vv);

    return 0;
}

void vector_manip(std::vector<int> vec){

    int i=0;
    std::vector<int> lv (10);

    for(i=0;i<10;i++){
        lv[i] = vec[i];
    }

    for(i=0;i<10;i++){
        std::cout << "LV :" << lv[i] << "\t" << "VEC :" << vec[i] << std::endl;
    }

    std::vector<int> lv2 (vec);

    for(i=0;i<10;i++){
        std::cout << "LV2 :" << lv2[i] << "\t" << "VEC :" << vec[i] << std::endl;
    }
}
