/*Test example looking at construction and destruction*/

#include <string>
#include <vector>
#include <iostream>

struct X {
    int val;
    int pers_check;

    void out(const std::string& s, int nv)
        { std::cerr << this << "->" << s << ": "<< val <<" (" << nv << ")\n";}

    X(){out("X()",0); val=0;}
    X(int v){val=v; out( "X(int)",v);}
    X(const X& x){ val=x.val; out("X(X&)",x.val); }
    X& operator=(const X& a)
        {out("X::operator=()",a.val); val=a.val; return *this; }
    ~X() {out("~X()",0); }
};

X glob(2);
X copy(X a) {return a;}
X copy2(X a) {X aa = a; return aa;}
X& ref_to(X& a) {return a;}
X* make(int i) {X a(i); return new X(a);}
struct XX {X a; X b;};

int main()
{
    std::cout<<"--------------------------------------------------"<<std::endl;
    X loc{4};
    //Standard Constructor
    std::cout<<"--------------------------------------------------"<<std::endl;
    X loc2{loc};
    //Copy Constructor
    std::cout<<"--------------------------------------------------"<<std::endl;
    loc = X{5};
    //Standard Constructor -> Copy Assignment -> Deleting RValue
    std::cout<<"--------------------------------------------------"<<std::endl;
    loc2 = copy(loc);
    //Copy Constructor(a{loc}) -> Rvalue Copy Constructor (For Return) ->
    //Copy Assignment -> Deleting Rvalue  -> Deleting a
    std::cout<<"--------------------------------------------------"<<std::endl;
    loc2 = copy2(loc);
    //Although not noticed due to a smart compiler, this would hopefully do
    //Copy Constructor(a{loc}) -> Copy Constructor(aa{a}) ->
    //Copy Constructor (for Return) -> Copy Assignment(loc2 = rvalue)
    //Deleting Rvalue -> Deleting AA -> Deleting A
    std::cout<<"--------------------------------------------------"<<std::endl;
    X loc3 {6};
    //Standard Constructor
    std::cout<<"--------------------------------------------------"<<std::endl;
    X& r = ref_to(loc);
    //No construction occurs as all are references
    std::cout<<"--------------------------------------------------"<<std::endl;
    delete make(7);
    //Standard Constructor -> Copy Constructor(new X{a}) ->
    //Deleting new X{a} -> Deleting a
    std::cout<<"--------------------------------------------------"<<std::endl;
    delete make(8);
    //Same as above
    std::cout<<"--------------------------------------------------"<<std::endl;
    std::vector<X> v(4);
    //Four standard constructions
    std::cout<<"--------------------------------------------------"<<std::endl;
    XX loc4;
    //Standard Construction of a standard construction with no initialisation
    std::cout<<"--------------------------------------------------"<<std::endl;
    X* p = new X{9};
    //Pointer to a standard Construction
    std::cout<<"--------------------------------------------------"<<std::endl;
    delete p;
    //Deleter of what p points to
    std::cout<<"--------------------------------------------------"<<std::endl;
    X* pp = new X[5];
    //Standard Construction of an array of 5 X
    std::cout<<"--------------------------------------------------"<<std::endl;
    delete[] pp;
    //Deleting array of 5 X
    std::cout<<"--------------------------------------------------"<<std::endl;
    std::cout<<"To prevent error from compiler "<<r.val<<std::endl;
    std::cout<<"--------------------------------------------------"<<std::endl;
    //Reference to the value of loc
    //At end of program all destructors are called to delete and loose memory

}
