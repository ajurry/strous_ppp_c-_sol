/*Basic cin manipulation, plus class experimentation*/

#include "std_lib_facilities.h"

string flash;

class Variable{
    public:
        string name;
        double value;
    Variable(string na, double val)
        :name(na), value(val) {}
};


int main()
{

    vector<Variable> var_table;

    var_table.push_back(Variable("word",565));

    cout << var_table[0].name <<" "<< var_table[0].value<<endl;

    cout<<"Input manipulation loop"<<endl;
    cout<<"Insert a word and the the first character will be stored"<<endl;
    cout<<"To exit loop type /"<<endl;
    char ch;
    while (cin) {
        cin >> ch;
        if(ch == '/'){
            return 0;
        }
        cin.putback(ch);
        cin >> flash;
        cout << ch << "--" << "--" << flash << '\n';
    }
    return 0;

}
