CHAP_DIR := $(notdir $(abspath $(dir $(lastword $(MAKEFILE_LIST)))))


PLAY_SRCS := ${CHAP_DIR}/chap06_ex_1_1.cpp
PLAY_OBJS := ${PLAY_SRCS:%.cpp=${OBJDIR}/%.o}

${BINDIR}/chap06_prog_1: ${PLAY_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${CXX} ${LXXFLAGS} $< -o $@


CALCULATOR_1_SRCS := ${CHAP_DIR}/chap06_ex_1_2.cpp
CALCULATOR_1_OBJS := ${CALCULATOR_1_SRCS:%.cpp=${OBJDIR}/%.o}

${BINDIR}/chap06_prog_2: ${CALCULATOR_1_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${CXX} ${LXXFLAGS} $< -o $@

TARGETS+=${BINDIR}/chap06_prog_1 \
		 ${BINDIR}/chap06_prog_2
