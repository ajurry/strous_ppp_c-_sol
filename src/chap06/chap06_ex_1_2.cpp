/*Basic calculator, that only uses addition and substraction*/

#include "std_lib_facilities.h"

int main()
{
    cout << "Please Enter Experssion ( we can handle + and - ): ";
    int lval = 0;
    int rval = 0;
    char op;
    int res;
    cin>>lval>>op>>rval;

    if(op=='+')
        res = lval + rval; //addition
    else if (op=='-')
        res = lval - rval; //subtraction

    cout<<"Result:"<<res<<'\n';
    keep_window_open();

    return 0;
}
