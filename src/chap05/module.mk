CHAP_DIR := $(notdir $(abspath $(dir $(lastword $(MAKEFILE_LIST)))))


EXCEPTIONS_SRCS := ${CHAP_DIR}/chap05_ex_1_1.cpp
EXCEPTIONS_OBJS := ${EXCEPTIONS_SRCS:%.cpp=${OBJDIR}/%.o}

${BINDIR}/chap05_prog_1: ${EXCEPTIONS_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${CXX} ${LXXFLAGS} $< -o $@


TARGETS+=${BINDIR}/chap05_prog_1

