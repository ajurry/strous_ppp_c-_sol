/*Basic area calculation program, using catch statement for negative or zero
values lengths*/

#include <iostream>
#include <string>

class Bad_area{};

int area(int length, int width);

int main()
{
    int area1 =0;
    int area2 =0;
    int area3 =0;
    int x;
    int y;
    int z;

    std::cout<<"Please Input X, Y, Z as ints"<<std::endl;
    std::cin>>x>>y>>z;

    try{

        area1 = area(z,y);
        area2 = area(z,x);
        area3 = area(y,x);
    }

    catch(Bad_area){
    std::cout<<"Whoops! Bad arguements in area()\n";
    return 0;
    }

        std::cout<<"Area (ZY) = "
        <<area1<<"\tArea (ZX) = "
        <<area2<<"\tArea (YX) = "
        <<area3<<"\n";

    return 0;
}

int area(int length, int width)
{
    if(length<=0||width<=0) throw Bad_area{};
    return length*width;
}
