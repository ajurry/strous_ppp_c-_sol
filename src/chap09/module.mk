CHAP_DIR:= $(notdir $(abspath $(dir $(lastword $(MAKEFILE_LIST)))))


QUESTION_1_SRCS := ${CHAP_DIR}/Exercises/Exercise1-4/chap09_ex_1_2.cpp
QUESTION_1_SRCS += ${CHAP_DIR}/Exercises/Exercise1-4/chap09_ex_1_1.cpp

QUESTION_1_OBJS := ${QUESTION_1_SRCS:%.cpp=${OBJDIR}/%.o}

${BINDIR}/chap09_prog_1: ${QUESTION_1_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


QUESTION_2_SRCS := ${CHAP_DIR}/Exercises/Exercise5-18/chap09_ex_2_4.cpp
QUESTION_2_SRCS += ${CHAP_DIR}/Exercises/Exercise5-18/chap09_ex_2_2.cpp
QUESTION_2_SRCS += ${CHAP_DIR}/Exercises/Exercise5-18/chap09_ex_2_1.cpp

QUESTION_2_OBJS := ${QUESTION_2_SRCS:%.cpp=${OBJDIR}/%.o}

${BINDIR}/chap09_prog_2: ${QUESTION_2_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


QUESTION_3_SRCS := ${CHAP_DIR}/Ver_9.4.1/chap09_ex_3_2.cpp
QUESTION_3_SRCS += ${CHAP_DIR}/Ver_9.4.1/chap09_ex_3_1.cpp

QUESTION_3_OBJS := ${QUESTION_3_SRCS:%.cpp=${OBJDIR}/%.o}

${BINDIR}/chap09_prog_3: ${QUESTION_3_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@


TARGETS+=${BINDIR}/chap09_prog_1 \
		 ${BINDIR}/chap09_prog_2 \
		 ${BINDIR}/chap09_prog_3

