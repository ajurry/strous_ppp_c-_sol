/*Basic date exercise, checking day, month, year.
Not robust*/

#include "chap09_ex_3_1.h"
#include <iostream>

int main(){

    Back::Date d1 = Back::Date(1950,5,5);

    Back::operator<<(std::cout, d1);

    d1 = Back::Date();

    Back::operator<<(std::cout, d1);

    return 99;
}
