/*Basic date exercise, checking day, month, year.
Not robust*/

#include "chap09_ex_3_1.h"
#include <iostream>

namespace Back {

    Date::Date(int yy, int mm, int dd)
        : y{yy}, m{mm}, d{dd}
    //Here we have the class which initialises to the date given
    {
        if (yy<1900 || yy>2100){
            std::cout << "Invalid Year, Setting To Default Date";
        }
        else if (mm<1 || mm>12){
            std::cout << "Invalid Month, Setting To Default Date";
        }
        else if (dd<1 || dd>31){
            std::cout << "Invalid Day, Setting To Default Date";
        }
        //Checks that do nothing at the moment
    }

    Date::Date()
        : y{2000}, m{1}, d{1}
    {
    }

std::ostream& operator<<(std::ostream& os, const Date& d)
{
    return os << '(' << d.year()
        << ',' << d.month()
        << ',' << d.day() << ')';
    //print out statement through the cout stream
}

}
