/*Using classes to look a and hold data, here the classes also have functions
attached to them*/

#include "chap09_ex_1_1.h"
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

namespace Data{

void Name_pairs::add_name_age(std::string nname, double aage){
    age.push_back(aage);
    name.push_back(nname);
}
//Appends vectors, allowing for the addition of elements

void Name_pairs::sort_name(){
    std::vector<std::string> sname = name;
    std::vector<double> sage = age;
    std::sort(name.begin(), name.end());

    int i=0;
    int j=0;
    int len=0;
    len = name.size();

    for(i=0;i<len;i++){
        for(j=0;j<len;j++){
            if(name[i]==sname[j]){
                age[i]=sage[j];
                break;
            }
        }
    }
}
//Really bad sorting mechanism, which the loop is extremely long

void print_name_age(const Name_pairs& np){

    int i=0;
    int len;
    len = np.get_size();

    for(i=0;i<len;i++){
       std::cout<<"Name : "<<np.get_name(i)
           <<"\t Age : "<<np.get_age(i)<<"\n";
    }
}
//Print function that is without a direct io interaction

std::ostream& operator<<(std::ostream& os, const Name_pairs& np){

    int i=0;

    for(i=0;i<np.get_size();i++){
        os << "Name: " << np.get_name(i)
            <<"\t Age: "<< np.get_age(i)<<"\n";
    }

    os << std::endl;
    return os;
}
//Print function that uses a direction io interaction

bool operator==(const Name_pairs& a, const Name_pairs& b){

    int i=0;

    if(a.get_size() != b.get_size()){return false;}

    for(i=0;i<a.get_size();i++){
        if((a.get_name(i) == b.get_name(i)
                && a.get_age(i) == b.get_age(i)) == false){return false;}
    }

    return true;
}


bool operator!=(const Name_pairs& a, const Name_pairs& b){
    return !(a==b);
}
}
