/*Using classes to look a and hold data, here the classes also have functions
attached to them*/

#include "chap09_ex_1_1.h"
#include <string>
#include <iostream>

int main(){

    std::string s;
    double a;
    int i=0;

    Data::Name_pairs np;
    Data::Name_pairs pp;

    for(i=0;i<10;i++){
    std::cout<<"Please enter a name"<<std::endl;
    std::cin>>s;
    std::cout<<"Please enter an age"<<std::endl;
    std::cin>>a;

    np.add_name_age(s,a);
    pp.add_name_age(s,a);

    std::cout<<"Would you like to stop?"<<std::endl;
    std::cin>>s;
    if(s=="y"||s=="Y"){
        break;
    }
    }

    std::cout<<"-----------------------"<<std::endl;
    Data::print_name_age(np);
    std::cout<<"-----------------------"<<std::endl;
    Data::operator<<(std::cout, np);
    std::cout<<"-----------------------"<<std::endl;

    std::cout<<"Would you like to sort?"<<std::endl;
    std::cin>>s;
    if(s=="y"||s=="Y"){
        np.sort_name();
        std::cout<<"-----------------------"<<std::endl;
        Data::print_name_age(np);
        std::cout<<"-----------------------"<<std::endl;
    }

    if(Data::operator==(np,pp) == true){
        std::cout<<"Named pairs are equal"<<std::endl;
    }

    if(Data::operator!=(np,pp) == true){
        std::cout<<"Named pairs are not equal"<<std::endl;
    }



    return 0;
}
