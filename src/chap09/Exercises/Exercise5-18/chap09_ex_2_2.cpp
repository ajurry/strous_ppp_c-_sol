/*Experimentation with classes once again, here the example is set around
book storage in libraries*/

#include "chap09_ex_2_3.h"
#include <iostream>
#include <string>

namespace Library{

    Patron::Patron(std::string NName, std::string NNumber,
            double BBalance)
        :Name{NName}, Number{NNumber}, Balance{BBalance}{}

    std::ostream& operator<<(const Patron& pp, std::ostream& os){

        os << "Name:" << "\t\t\t" << pp.get_Name() << "\n"
            << "Number:" << "\t\t\t" << pp.get_Number() << "\n"
            << "Balance:" << "\t\t" << pp.get_Balance() << std::endl;

        return os;
    }

    bool operator<(const Patron& pp, int a){
        return (pp.get_Balance() < a);
    }

}
