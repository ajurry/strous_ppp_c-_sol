/*Experimentation with classes once again, here the example is set around
book storage in libraries*/

#include "chap09_ex_2_3.h"
#include <vector>
#include <string>
#include <iostream>

namespace Library{

        Book::Book(std::string IISBN, std::string ttitle,
                std::string aauthor, std::string ccdate,
                Genre ggen , int ccheck)
            :ISBN{IISBN}, title{ttitle}, author{aauthor}, cdate{ccdate},
            gen{ggen}, check{ccheck}
        {
            while(!is_ISBN(ISBN)){
                std::cout<<"Please Input A Valid ISBN"<<std::endl;
                std::cin>>ISBN;
            }
        }

        std::string Book::get_gen() const{

            std::string s;

            switch(gen){
                case Genre::fiction : s = "Fiction"; break;
                case Genre::nonfiction : s = "Nonfiction"; break;
                case Genre::periodical : s = "Periodical"; break;
                case Genre::biography : s = "Biography"; break;
                case Genre::children : s = "Children"; break;
            }

            return s;

        }

        bool is_ISBN(std::string ISBN){

            int i;
            int j=0;
            int counter=0;

            for(i=0;i<int(ISBN.size());++i){

                if(ISBN[i] == '-'){
                    for(j=counter;j<i;j++){
                        if(int(ISBN[j])<48 || int(ISBN[j])>57){
                            std::cout<<"Invalid Character: "<<ISBN[j]<<std::endl;
                            return false;
                        }
                    }
                    counter = i+1;
                }
            }

            int end = int(ISBN.size()) - 1;

            if(!(int(ISBN[end])<48 || int(ISBN[end])>57))
                return true;
            else if(!(int(ISBN[end])<65 || int(ISBN[end])>90))
                return true;
            else if(!(int(ISBN[end])<97 || int(ISBN[end])>122))
                return true;
            else{
                std::cout << "Invalid Character: "<<ISBN[end]<<std::endl;
                return false;
            }
        }

        std::ostream& operator<<(std::ostream& os, const Book& bb){

            os << "ISBN:" << "\t\t\t" << bb.get_ISBN() << "\n"
                << "Title:" << "\t\t\t" << bb.get_title() << "\n"
                << "Author:" << "\t\t\t" << bb.get_author() << "\n"
                << "Copyright Date:" << "\t\t" << bb.get_cdate() << "\n"
                << "Checked Out:" << "\t\t" << bb.get_check() << "\n"
                << "Genre:" << "\t\t\t" << bb.get_gen()
                << std::endl;

            return os;
        }

        bool operator==(const Book& a, const Book& b){
            return (a.get_ISBN() == b.get_ISBN());
        }

        bool operator!=(const Book& a, const Book& b){
            return !(a==b);
        }

        void checking (char a, const Book& bb){

            if((a == 'Y' || a == 'y') && bb.get_check() == 0){
                std::cout<<"Book Has Now Been Checked Out"<<std::endl;
            }
            else if((a == 'Y' || a == 'y') && bb.get_check() == 1){
                std::cout<<"Book Is Already Checked Out"<<std::endl;
            }
            else if((a == 'N' || a == 'n') && bb.get_check() == 1){
                std::cout<<"Book Has Now Been Checked In"<<std::endl;
            }
            else if((a == 'N' || a == 'n') && bb.get_check() == 0){
                std::cout<<"Book Is Already Checked In"<<std::endl;
            }
            else {std::cout<<"Invalid Input"<<std::endl;}
        }
}
