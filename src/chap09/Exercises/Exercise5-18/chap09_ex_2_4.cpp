/*Experimentation with classes once again, here the example is set around
book storage in libraries*/

#include "chap09_ex_2_3.h"
#include <iostream>

int main(){

    Library::Book LOTR("343-335-435-!","LORD_OF_THE_RINGS","TOLKIEN","DATE",
            Library::Genre::fiction, 0);

    Library::Book HGTTG("343-335-435-b","HITCHHICKERS_GUIDE_TO_THE_GALAXY",
            "ADAMS","DATE", Library::Genre::children, 1);

    Library::operator<<(std::cout, LOTR);

    if(Library::operator==(LOTR, HGTTG)){
        std::cout<<"They are the same"<<std::endl;
    }

    if(Library::operator!=(LOTR, HGTTG)){
        std::cout<<"They are different"<<std::endl;
    }

    Library::Patron Andy("Andy", "1234", -10);

    if(Library::operator<(Andy, 0)){
        std::cout<<"Client in debt"<<std::endl;
    }

    Library::operator<<(Andy, std::cout);

    Library::checking('Y', LOTR);
    Library::checking('N', LOTR);
    Library::checking('y', HGTTG);
    Library::checking('n', HGTTG);

}
