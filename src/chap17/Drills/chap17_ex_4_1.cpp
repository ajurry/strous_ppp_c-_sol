/*Exercise with array and storing data*/

#include <string>
#include <iostream>

void arrays();
void print_array(std::ostream& os, int *a, int size);
void set_array(int *a, int size);

int main(){

    int* p1 = new int{7};
    int* p2 = new int[7]{1,2,4,8,16,32,64};
    int* p3 = p2;

    std::cout<<p1<<std::endl;
    print_array(std::cout, p1, 1);

    std::cout<<p2<<std::endl;
    print_array(std::cout, p2, 7);

    p3 = p2;
    p2 = p1;

    std::cout<<p1<<std::endl;
    print_array(std::cout, p1, 1);

    std::cout<<p2<<std::endl;
    print_array(std::cout, p2, 1);

    delete p1;
    delete[] p3;

    int* p4 = new int[10]{1,2,4,8,16,32,64,128,256,512};
    int* p5 = new int[10];

    int i=0;

    for(i=0;i<10;i++){
        p5[i] = p4[i];
    }

    print_array(std::cout, p4, 10);
    print_array(std::cout, p4, 10);

    delete[] p4;
    delete[] p5;

    arrays();
    return 0;
}

void arrays(){

    int size10=10;
    int* Array10 = new int[size10];
    int size11=11;
    int* Array11 = new int[size11];
    int size20=20;
    int* Array20 = new int[size20];

    set_array(Array10, size10);
    print_array(std::cout, Array10, size10);
    set_array(Array11, size11);
    print_array(std::cout, Array11, size11);
    set_array(Array20, size20);
    print_array(std::cout, Array20, size20);

    delete[] Array10;
    delete[] Array11;
    delete[] Array20;

}

void print_array(std::ostream& os, int* a, int size){

    int i=0;

    for(i=0;i<size;i++){
        std::cout<<a[i]<<std::endl;
    }
}

void set_array(int* a, int size){

    int i=0;

    for(i=0;i<size;i++){
        a[i] = 100 + i;
    }
}
