CHAP_DIR:= $(notdir $(abspath $(dir $(lastword $(MAKEFILE_LIST)))))


QUESTION_1_SRCS := ${CHAP_DIR}/Test/Test_1/chap17_ex_1_1.cpp
QUESTION_1_OBJS := ${QUESTION_1_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap17_prog_1: ${QUESTION_1_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_2_SRCS := ${CHAP_DIR}/Test/Test_2/chap17_ex_2_1.cpp
QUESTION_2_OBJS := ${QUESTION_2_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap17_prog_2: ${QUESTION_2_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_3_SRCS := ${CHAP_DIR}/Test/Test_3/chap17_ex_3_1.cpp
QUESTION_3_OBJS := ${QUESTION_3_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap17_prog_3: ${QUESTION_3_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_4_SRCS := ${CHAP_DIR}/Drills/chap17_ex_4_1.cpp
QUESTION_4_OBJS := ${QUESTION_4_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap17_prog_4: ${QUESTION_4_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_5_SRCS := ${CHAP_DIR}/Exercises/Exercise_1/chap17_ex_5_1.cpp
QUESTION_5_OBJS := ${QUESTION_5_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap17_prog_5: ${QUESTION_5_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_6_SRCS := ${CHAP_DIR}/Exercises/Exercise_2/chap17_ex_6_1.cpp
QUESTION_6_OBJS := ${QUESTION_6_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap17_prog_6: ${QUESTION_6_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_7_SRCS := ${CHAP_DIR}/Exercises/Exercise_3/chap17_ex_7_1.cpp
QUESTION_7_OBJS := ${QUESTION_7_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap17_prog_7: ${QUESTION_7_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_8_SRCS := ${CHAP_DIR}/Exercises/Exercise_4/chap17_ex_8_1.cpp
QUESTION_8_OBJS := ${QUESTION_8_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap17_prog_8: ${QUESTION_8_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

QUESTION_10_SRCS := ${CHAP_DIR}/Exercises/Exercise_6/chap17_ex_10_1.cpp
QUESTION_10_SRCS += ${CHAP_DIR}/Exercises/Exercise_6/chap17_ex_11_1.cpp
QUESTION_10_OBJS := ${QUESTION_10_SRCS:%.cpp=${OBJDIR}/%.o}
${BINDIR}/chap17_prog_10: ${QUESTION_10_OBJS}
	@echo "LN $(@F)"
	@mkdir -p $(@D)
	@${LINK} $^ -o $@

TARGETS+=${BINDIR}/chap17_prog_1 \
		 ${BINDIR}/chap17_prog_2 \
		 ${BINDIR}/chap17_prog_3 \
		 ${BINDIR}/chap17_prog_4 \
		 ${BINDIR}/chap17_prog_5 \
		 ${BINDIR}/chap17_prog_6 \
		 ${BINDIR}/chap17_prog_7 \
		 ${BINDIR}/chap17_prog_8 \
		 ${BINDIR}/chap17_prog_10
