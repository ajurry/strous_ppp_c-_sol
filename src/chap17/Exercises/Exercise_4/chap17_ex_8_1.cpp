/*C-style, taking user input and storing it in a char array until the user
enters a !*/

#include <iostream>
#include <iomanip>

int main () {

    int size=512;
    int point=0;

    char* ptr_store = new char[size];
    char element;

    while(std::cin>>element){

        if(element=='!'){
            break;
        }

        ptr_store[point] = element;
        ++point;
    }

    for(int i=0;i<point;++i)
        std::cout<<ptr_store[i]<<std::endl;

    delete[] ptr_store;
    return 0;
}
