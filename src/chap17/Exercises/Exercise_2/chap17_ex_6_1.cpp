/*C-style way to copy a char array and then check for a copy of the first character
inside the char array*/

#include <iostream>

void print_array(char* a, int s);
void cpy_array(char* a, char* b, int s);
void check_string(char* a, char* b, int s1, int s2);

int main () {

    int size = 20;
    char* array = new char[size]{'H','e','l','l','o',' ','W','o','r','l','d','!','\0'};
    print_array(array,size);

    char* ptr = (char*)malloc(size * sizeof(char));
    cpy_array(array, ptr, size);
    print_array(ptr,size);

    int check_size = 1;
    char* ptr2 = (char*)malloc(check_size * sizeof(char));
    ptr2[0] = 'o';

    check_string(ptr,ptr2,size,check_size);

    delete[] array;
    free(ptr);
    free(ptr2);
    return 0;
}

void print_array(char* a, int s){

    int i=0;
    for(i=0;i<s;i++){
        std::cout<<a[i]<<std::endl;
        if(a[i] == '\0'){
            return;
        }
    }
}

void cpy_array(char* a, char* b, int s){

    int i=0;

    for(i=0;i<s;i++){
        b[i] = a[i];
    }
}

void check_string(char* a, char* b, int s1, int s2){

    int i=0;
    int j=0;
    bool check;

    for(i=0;i<(s1-s2)+1;i++){
        check = true;
        for(j=0;j<s2;j++){
            if(b[j]!=a[i+j]){
                check = false;
            }
            if(check){
                std::cout<<"We have a copy at: "<<i<<std::endl;
                return;
            }
        }
    }
}
