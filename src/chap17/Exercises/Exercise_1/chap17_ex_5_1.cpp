/*Lowering the case of a char array in a c-style manner, without
the use of std::library functions*/

#include <iostream>

void to_lower(char* a, int s);
void print_array(char* a, int s);

int main () {

    int size = 20;

    char* array = new char[size]{'H','e','l','l','o',' ','W','o','r','l','d','!','\0'};
    print_array(array,size);

    to_lower(array,size);
    print_array(array,size);

    delete[] array;
}

void to_lower(char* a, int s){

    int i = 0;
    int buffer=0;

    for(i=0;i<s;i++){
        if(a[i] != '\0'){
            if(!(a[i] < 65 || a[i] > 90)){
                buffer  = a[i];
                a[i] = buffer + 32;
            }
        }
        else if(a[i] == '\0'){
            return;
        }
        else{
            std::cout<<"ERROR"<<std::endl;
        }
    }
}

void print_array(char* a, int s){

    int i=0;
    for(i=0;i<s;i++){
        std::cout<<a[i]<<std::endl;
        if(a[i] == '\0'){
            return;
        }
    }
}
