/*Basic custom double linked list*/

#include <string>
#include <iostream>

#include "chap17_ex_10_1.h"


int main() {

    Link* n_g = new Link{"Thor","Norse","Horse","Hammer"};
    n_g = n_g->insert(new Link{"Odin","Norse","Horse","Spear"});
    n_g = n_g->insert(new Link{"Freia","Norse","Horse","Spear"});

    print_all(n_g);
    std::cout<<std::endl;


    return 0;

}
