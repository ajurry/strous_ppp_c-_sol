/*Basic custom double linked list*/

#include "chap17_ex_10_1.h"
#include <string>
#include <iostream>

Link* Link::insert(Link* n)
{
    if (n==nullptr) return this;
    if (this==nullptr) return n;
    n->succ = this;
    if(this->prev) this->prev->succ = n;
    n->prev = this->prev;
    return n;
}

Link* Link::add(Link* n){

    if (n==nullptr) return this;
    if (this==nullptr) return n;
    n->prev=this;
    if (this->succ) this->succ->prev=n;
    n->succ = this->succ;
    this->succ = n;
    return n;
}

Link* Link::erase(){

    if (this==nullptr) return nullptr;
    if (this->succ) this->succ->prev = this->prev;
    if (this->prev) this->prev->succ = this->succ;
    return this->succ;
}

Link* Link::find(const std::string& s){

    Link* p = this;

    while(p){
        if (p->details.name == s) return p;
        p = p->succ;
    }
    return nullptr;
}

Link* Link::advance(int n) const{

    Link* p = const_cast<Link*>(this);
    if(this==nullptr) return nullptr;
    if(0<n){
        while (n--){
            if(p->succ==nullptr) return nullptr;
            p = p->succ;
        }
    }
    else if(n<0){
        while(n++){
            if (p->prev == nullptr) return nullptr;
            p = p->prev;
        }
    }

    return p;
}

void print_all(Link* p){

    while(p) {
        std::cout<< "{";
        std::cout << p->details.name << "," <<p->details.mythology <<","
            << p->details.vehicle << "," << p->details.weapon;
        p = p->next();
        if(p){
            std::cout<<"}"<<"\n";
        }
        }
    std::cout<<"}"<<std::endl;
}
