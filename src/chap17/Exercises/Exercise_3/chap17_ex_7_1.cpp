/*Exercise for checking what happens upon memory exhaustion*/

#include <iostream>
#include <string>

int main () {

    int i=0;

    while(true){
        double* ptr = new double[1000]{};
        i++;

        if((i%1000)==0){
            std::cout<<i<<" "<<ptr<<std::endl;
        }
    }


    return 0;
}
