/*Looking at the sizes of specific variables*/

#include <iostream>
#include <string>


int main(){

    int i = 1;
    int *a = &i;
    double j = 2;
    double *b = &j;
    bool k = true;
    bool *c = &k;
    char l = '2';
    char *d = &l;
    std::string m = "Word";
    std::string *e = &m;

    std::cout << "int" <<"\t"<< "the size of " << sizeof(int) << " " << sizeof(i) << "\t\t" << i <<std::endl;
    std::cout << "int*" <<"\t"<<"the size of " << sizeof(int*) << " " << sizeof(a) << "\t\t" << *a << std::endl;
    std::cout << "double" <<"\t"<<"the size of " << sizeof(double) << " " << sizeof(j)<< "\t\t" << j  << std::endl;
    std::cout << "double*" <<"\t"<<"the size of " << sizeof(double*) << " " << sizeof(b)<< "\t\t" << *b  << std::endl;
    std::cout << "bool" <<"\t"<<"the size of " << sizeof(bool) << " " << sizeof(k)<< "\t\t" << k  << std::endl;
    std::cout << "bool*" <<"\t"<<"the size of " << sizeof(bool*) << " " << sizeof(c)<< "\t\t" << *c  << std::endl;
    std::cout << "char" <<"\t"<<"the size of " << sizeof(char) << " " << sizeof(l)<< "\t\t" << l  << std::endl;
    std::cout << "char*" <<"\t"<<"the size of " << sizeof(char*) << " " << sizeof(d)<< "\t\t" << *d  << std::endl;
    std::cout << "string" <<"\t"<<"the size of " << sizeof(std::string) << " " << sizeof(m)<< "\t" << m  << std::endl;
    std::cout << "string*" <<"\t"<<"the size of " << sizeof(std::string*) << " " << sizeof(e)<< "\t" << *e  << std::endl;

    return 0;

}
