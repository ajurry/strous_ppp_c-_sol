/*Program to look at pointers inside structs, looking at double linked
list style of construct*/

#include <iostream>
#include <string>

struct integer {
    int number;
    integer* before;
    integer* after;
    integer(const int& n, integer* b=nullptr,integer* a=nullptr)
        : number{n}, before{b}, after{a}{}
    ~integer(){std::cout << "Deleting" << std::endl;}
};

int main(){

    integer* numbers = new integer{10,nullptr,nullptr};
    numbers = new integer{20,nullptr,numbers};
    numbers->after->before = numbers;
    numbers = new integer{40,nullptr,numbers};
    numbers->after->before = numbers;

    std::cout << numbers->number << std::endl;
    std::cout << numbers->after->number << std::endl;
    std::cout << numbers->after->after->number << std::endl;
    std::cout << numbers->after->after->before->number << std::endl;
    std::cout << numbers->after->after->before->before->number << std::endl;

    delete numbers;
    return 0;
}
