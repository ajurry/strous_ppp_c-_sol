/*Further exercies regarding the production of memory on heap and its deletion*/

#include <iostream>
#include <string>
#include <vector>

class Food_Types {

public:
    Food_Types(int n)
        :number{n}, types{new std::string[n]}
    {
        std::cout<<"Created: "<<n<<" On Heap"<<std::endl;
    }

    ~Food_Types(){
        delete[] types;
        std::cout<<"Deleted: "<<number<<" Off Heap"<<std::endl;
    }

    void Add_Types(){
        int i=0;
        for(i=0;i<number;i++){
            std::cout<<"Please Input A Type ";
            std::cin>>types[i];
        }
    }

    void Print_Types(){
        int i=0;
        for(i=0;i<number;i++){
            std::cout<<"Element: "<<types[i]<<std::endl;
        }
    }

private:
    int number;
    std::string* types;
};

int main(){

    Food_Types* Apples = new Food_Types(4);

    Apples->Add_Types();
    Apples->Print_Types();

    delete Apples;

    return 0;
}
